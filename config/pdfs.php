<?php

return [

    /*
    | PDF Generation Settings
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'path' => [
        'temp' => env('UPLOAD','tmp').'/',
		'pdfs' => env('PDF','pdfs')
    ],

    'key' => [
        'google' => env('API_KEY'),
    ],
	
	'tools' => [
		'mutool' => env('MUTOOL'),
		'pdfinfo' => env('PDFINFO'),
		'pdfunite' => env('PDFUNITE'),
	],
	
	'limit'	=>	[
		'size'	=>	env('FILESIZE_LIMIT')
	]

];
