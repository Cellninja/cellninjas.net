<?php

return [
	'PAGE'=>[
			'PAGE_START'=>0,
			'PAGE_MAX'=>513
		],
	'DATA_HEADERS'=>[
			'A1'=>'Item',
			'B1'=>'Conn. Date',
			'C1'=>'Conn. Time (UTC)',
			'D1'=>'Originating Number',
			'E1'=>'ET',
			'F1'=>'Bytes Up',
			'G1'=>'Bytes Dn',
			'H1'=>'IMEI',
			'I1'=>'IMSI',
			'J1'=>'MAKE',
			'K1'=>'MODEL',
			'L1'=>'Cell Location'
		],
	'TEXT_HEADERS'=>[
			'A1'=>'Item',
			'B1'=>'Conn. Date',
			'C1'=>'Conn. Time (UTC)',
			'D1'=>'Originating Number',
			'E1'=>'Terminating Number',
			'F1'=>'IMEI',
			'G1'=>'IMSI',
			'H1'=>'Desc',
			'I1'=>'Feature',
			'J1'=>'Cell Location'
		],
	'TALK_HEADERS'=>[
			'A1'=>'Item',
			'B1'=>'Conn. Date',
			'C1'=>'Conn. Time (UTC)',
			'D1'=>'Seizure Time',
			'E1'=>'ET',
			'F1'=>'Originating Number',
			'G1'=>'Terminating Number',
			'H1'=>'IMEI',
			'I1'=>'IMSI',
			'J1'=>'CT',
			'K1'=>'Feature',
			'L1'=>'Cell Location'
		],
	];
?>
