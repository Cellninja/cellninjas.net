<div style="width:100%;text-align:center;padding:30px 0;">
	<img width="200px" height="auto" src="{{ $appurl.'/images/logo.png' }}" />
</div>
<div style="background: #cccccc47; padding: 30px 50px;">
	<p>Your {{ $appname }} file is completed.  You may download it at the following link</p>
	<p><a href="{{$download}}">{{$download}}</a></p>
	<br><br>
	<p>Visit the following link for user instructions:</p>
	<p><a href="https://upload.cell.ninja/documentation/cell_ninja_user_guide_v1-0.pdf">https://upload.cell.ninja/documentation/cell_ninja_user_guide_v1-0.pdf</a>
</div>