Your {{ $appname }} file is completed.  You may download it at the following link
{{$download}}
Visit the following link for user instructions:
https://upload.cell.ninja/documentation/cell_ninja_user_guide_v1-0.pdf