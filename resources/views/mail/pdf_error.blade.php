<div style="width:100%;text-align:center;padding:30px 0;">
	<img width="200px" height="auto" src="{{ $appurl.'/images/logo.png' }}" />
</div>
<div style="background: #cccccc47; padding: 30px 50px;">
	<p>Sorry, your file was unable to be processed due to the following reason:</p>
	<p>{{$error}}</p>
	<br><br>
</div>