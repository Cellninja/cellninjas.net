<script type="text/javascript">
    $('[name="enabledToggle"]').change(function (e) {
        const form = $(this).closest('form')[0];

        // the toggle controls value doesn't get properly sent with the form, nor does it properly updated the enabled
        // value on the model, so I am taking the checked value from the toggle and applying its value to a hidden enabled
        // field on the form -- Eric Smallwood
        form[3].value = form[1].checked ? 1 : 0;
        form.submit();
    });
</script>
