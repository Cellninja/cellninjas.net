@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					<div class="row">
						<div class="col-md-3">
							<label for="name">Case Name:</label><br/>
							<input id="name" class="w-100" name="name" type="text"/>
						</div>
						<div class="col-md-3">
							<label for="date">Incident Date:</label><br/>
							<input id="date" type="date" placeholder="MM/DD/YYYY" class="w-100" name="date" type="text"/>
						</div>
						<div class="col-md-2 pr-0">
							<label for="time">Local Time:</label><br/>
							<input id="time" type="time" placeholder="HH:MM AM/PM" class="w-100" name="time" type="text"/>
						</div>
						<div class="col-md-2 pr-0">
							<label for="name">Carrier:</label><br/>
							<select id="carrier" class="w-100" name="carrier">
								<option value="att">AT&amp;T</option>
								<option value="sprint">Sprint</option>
								<option value="tmobile">TMobile</option>
								<option value="verizon">Verizon</option>
							</select>
						</div>
                       <div class="col-md-2">
							<label for="name">Output Type:</label><br/>
							<select id="mode" class="w-100" name="mode">
								<option value="pdf">PDF</option>
								<option value="excel">Excel</option>
							</select>
						</div>
						<div class="col-md-2 d-none">
							<label for="name">Input Object:</label><br/>
							<select id="source" class="w-100" name="source">
								<option value="records">records</option>
								<option value="towers">towers</option>
							</select>
						</div>
					</div>
                    <upload-component  ref="profile"></upload-component>
                    <div>
                    <a href="">Load another file</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
