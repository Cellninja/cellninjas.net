<?php

use Illuminate\Database\Seeder;

class DummyPlan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'title' => 'Standard',
            'stripe_prod_id' => 1,
			'stripe_plan_id' => 1,
        ]);
        DB::table('plans')->insert([
            'title' => 'Premium',
            'stripe_prod_id' => 1,
			'stripe_plan_id' => 2,
        ]);
    }
}
