<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Michael',
            'email' => 'mricotta@arcanestrategies.com',
            'password' => bcrypt('paxx'),
            'plan_id' => 1,
            'enabled' => true
        ]);

        DB::table('users')->insert([
            'name' => 'Denis',
            'email' => 'denis@mcduffbc.com',
            'plan_id' => 1,
            'password' => bcrypt('paxx'),
        ]);

        DB::table('users')->insert([
            'name' => 'Benlev',
            'email' => 'benlev@aol.com',
            'plan_id' => 1,
            'password' => bcrypt('paxx'),
        ]);

        DB::table('users')->insert([
            'name' => 'Eric',
            'email' => 'eric@joviantechnologies.io',
            'plan_id' => 1,
            'password' => bcrypt('paxx'),
        ]);
    }
}
