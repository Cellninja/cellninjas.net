<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAzimuth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('sprint_towers', function (Blueprint $table) {
			$table->integer('azimuth')->default(0)->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('sprint_towers', function (Blueprint $table) {
			$table->integer('azimuth')->default(null)->change();
		});
    }
}
