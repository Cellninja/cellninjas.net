<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SprintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('sprint_towers', function (Blueprint $table) {
			$table->integer('id');
			$table->string('cascade');
			$table->string('switch');//->default('HSTNTXHN-MSCE-2');
			$table->integer('neid');//->default('205');
			$table->string('latitude');
			$table->string('longitude');
			$table->string('bts');//->default('Ericsson');
			$table->integer('sector');//->default('1');
			$table->integer('azimuth');//->default('0');
			$table->string('cdr');//->default('Nonactive');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sprint_towers');
    }
}
