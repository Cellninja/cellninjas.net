<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('users', function (Blueprint $table) {
			$table->unsignedBigInteger('plan_id')->default(1);
			$table->string('code')->nullable();
			//$table->foreign('plan_id')->references('id')->on('plans');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('plan_id');
			$table->dropColumn('code');
			//$table->dropForeign(['plan_id']);
		});
    }
}
