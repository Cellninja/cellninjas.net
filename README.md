# Be sure to set public folder permissions to 664 in order to allow the file concatenation.
# sudo setfacl -m "d:g:www-data:rwx" /var/www/html/beta.cellninjas.net/storage/app/public/
1. Enable fileinfo extension in php.ini
2. Enable pdo_mysql extension in php.ini
3. Run command `composer upgrade`
   1. If you see the error:<br />
      `In PackageManifest.php line 131:`<br />
      `Undefined index: name` <br /> 
      `Script @php artisan package:discover --ansi handling the post-autoload-dump event returned with error code 1` <br />
   Then update PackageManifest.php file by changing this line: <br />
   `$packages = json_decode($this->files->get($path), true);` <br />
   to this: <br />
   `$installed = json_decode($this->files->get($path), true);`<br />
   `$packages = $installed['packages'] ?? $installed;` <br />
   2. If you see the error: <br />
   `InvalidArgumentException` <br />
   `Please provide a valid cache path.` <br />
   Add the cache folders with the following commands: <br />
   `mkdir logs` <br />
   `mkdir framework`<br />
   `mkdir framework/cache && framework/cache/data`<br />
   `mkdir framework/sessions`<br />
   `mkdir framework/testing`<br />
   `mkdir framework/views`<br />
4. Run command `yarn install`
5. Download mutool https://www.mupdf.com/downloads/index.html
6. Download poppler https://github.com/oschwartz10612/poppler-windows/releases/tag/v21.03.0
7. Update .env file with the location of mutool as well as pdfunite and pdfinfo from poppler
8. Install mysql
   1. update .env file with your local mysql credentials
   2. create ninja database `CREATE DATABASE ninja;`
9. run command `php artisan migrate`
10. run command `php artisan db:seed`
11. run command `php artisan serve`
