<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail as Mail;
use App\VerizonTowers as VerizonTowers;
use Storage;
use URL;

class ImportVerizonTowers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'import:verizontowers {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports a CSV of verizon cell towers to the database.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$fileName = $this->argument('filename');
		$file = self::importXLS($fileName);
		return $file;
    }
	
	public function importCSV($fileName='verizon_cell_towers.xlsx')
	{
		// REFACTOR: This will need to handle uploads too.
		$filePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'public/'.$fileName;
		$file = fopen($filePath, 'r');
		$importData_arr = $headers = [];
		$i = 0;

		while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			$num = count($filedata);
             
             // first row is headers
             if($i == 0){
				$headers = $filedata;
                $i++;
                continue; 
             }
			 // REFACTOR: Array walk or array map headers to rows instead of 2 loops.
             for ($c=0; $c < $num; $c++) {
				$insert = $filedata[$c];
				$hdr = $headers[$c];
				$hdr = str_replace(['cell','id#','cell#'],['id','id','id'],strtolower($hdr));
                $headers[$c] = $hdr;
				$importData_arr[$i][$hdr] = $insert;
             }
             $i++;
		}
		fclose($file);

		foreach($importData_arr as $importData){
			$insertData = [];
			foreach($headers as $h){
				if(!empty($h)){
					$insertData[$h]=$importData[$h];
				}
			}
			SprintTowers::firstOrCreate($insertData);
		}
		Echo($i.' rows imported');
		
		return true;
    }

	public function importXLS($fileName=null)
	{
		if(empty($fileName)){
			return false;
		}
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$first = $reader->listWorksheetNames($fileName)[0];
		//$reader->setReadDataOnly(true);
		$reader->setLoadSheetsOnly($first);
		$spreadsheet = $reader->load($fileName);
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		$headers = array_filter($sheetData[1]);

		foreach($sheetData as $row=>$data){
			$data = array_filter($data, function($value) { return !is_null($value) && $value !== ''; });
			if(empty($data)||$row<2){
				continue;
			}
			$cols = 10;
			if(count($headers)>$cols){
				return false;
			}
			$insertData = [];
			foreach($data as $k=>$d){
				if(!empty($headers[$k])){
					$h = str_replace(['cell','id#','cell#'],['id','id','id'],strtolower($headers[$k]));
					$insertData[$h] = $d;
				}	
			}
			SprintTowers::firstOrCreate($insertData);
		}		
		return true;
    }
}
