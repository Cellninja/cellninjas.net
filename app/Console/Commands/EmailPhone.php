<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail as Mail;
use App\Mail\AdminMessage as AdminMessage;
use App\Phone as Phone;
use Storage;
use URL;

class EmailPhone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'email:phone {address}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emails a list of all phone numbers to a provided email address.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$address = $this->argument('address');
		$file = self::exportCSV();
		$file = URL::to('/storage/'.$file);
		return Mail::to($address)->send((new AdminMessage($file)));
    }
	
	public function exportCsv()
	{
		$fileName = 'phones.csv';
		$filePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'public/'.$fileName;
		$phones = Phone::all()->pluck('phone')->toArray();
		$file = fopen($filePath, 'w');
		fputcsv($file, $phones);
		fclose($file);
        return $fileName;
    }
	
}
