<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail as Mail;
use App\Mail\MessageFile as MessageFile;
use App\Http\Controllers\PdfController as PdfController;
use Storage;
use URL;

class ProcessPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'process:pdf {original} {mode} {carrier} {address} {case} {date} {time} {timestamp}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes an uploaded file with parameter of filetype.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	   	$process = new PdfController();
	    $filepath = $this->argument('original');
		$mode = $this->argument('mode');
		$carrier = $this->argument('carrier');
		$address = $this->argument('address');
		if($carrier==='sprint'){
			$final = 'Sorry, Sprint does not permit for PDF file parsing.';
		}
		$case = $this->argument('case');
		$date = $this->argument('date')!=="null"? $this->argument('date') : null;
		$time = $this->argument('time')!=="null"? $this->argument('time') : null;
		$timestamp = $this->argument('timestamp')!=="null"? $this->argument('timestamp') : null;
		$process->getRawHTML($filepath);
		$file = $process->getProcessedFile($filepath,$mode,$carrier,$case,$date,$time,$timestamp);
		if($file==false){
			if($carrier == 'att'){
				$final = 'There is a formatting issue with AT&T’s pdf file. To resolve this issue, request a csv file format from AT&T, and run this report again';
			} else if($carrier == 'sprint'){
				$final = 'Your file contains invalid formatting or missing cell towers. Please upload your cell towers file (XLS/CSV) and check to see that you have the correct columns in your report.';
			}
		} else {
			$final = Storage::disk('s3')->url(config('services.s3.output').$file);
		}
		return Mail::to($address)->send((new MessageFile($final)));
    }
}
