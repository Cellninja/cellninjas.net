<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerizonTowers extends Model
{
    protected $table = 'verizon_towers';
	public $incrementing = false;
	public $timestamps = false;
    protected $fillable = [
        'id','cascade','switch','neid','latitude','longitude','bts','sector','azimuth','cdr'
    ];
}
