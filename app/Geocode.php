<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Geocode extends Model
{
    protected $table = 'geocode';
	public $incrementing = true;
    protected $fillable = [
        'latlng', 'response'
    ];
	
}
