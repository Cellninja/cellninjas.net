<?php

namespace App\Http\Middleware;

use Closure;

class AuthGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $pos = strpos($routeName, 'users');

        if(($pos || $pos === 0) && auth()->user()->plan_id !== 0) {
            return redirect('/');
        }

        return $next($request);
    }
}
