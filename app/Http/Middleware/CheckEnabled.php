<?php

namespace App\Http\Middleware;

use Closure;

class CheckEnabled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->enabled == false){

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route('login')->withErrors(['disabled' => 'Your Account has been disabled, please contact the administrator.']);
        }

        return $next($request);
    }
}
