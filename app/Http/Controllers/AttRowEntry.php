<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use App\Phone as Phone;

class AttRowEntry {
    protected $V_ITEM = '[\d]+\s\s'; // Checks for FIRST numeric string before space.
	protected $V_CONN_DATE= '\d{2}\/\d{2}\/\d{2}'; // Checks for FIRST exact match date string.
	protected $V_CONN_TIME= '\d{2}\:\d{2}\:\d{2}'; // Checks for FIRST exact match time string.
	protected $V_HHMM= '[\s]\d{1,3}\:(\d{2}|\d{1}\s*\d{1}\s)'; // Matches an HH:MM timestamp where HH can be H or HH
	protected $V_SEIZURE= ['V_HHMM'];
	protected $V_ET= ['V_HHMM'];
	protected $V_FORWARDING = '\s\d{7,14}((\([FDfd]\))|(\(OO\)))\s';
	protected $V_EXTENDEDPHONE = '\s((\d{2,14})|(-1))\s';
	protected $V_PHONE = '\s\d{11}\s';
	protected $V_ORIGIN= ['V_PHONE'];
	protected $V_TERM= ['V_PHONE'];
	protected $V_IMEI= '\s\d{16}'; // This typically begins with a number and then continues on to take any variation of characters
	protected $V_IMSI= '\s\d{15}\s';
	protected $V_CT= '[\s][A-Z]{2}\s';
	protected $V_BRACK = '\[(.*?)\]'; // Matches text between brackets
	protected $V_MMS= '\b(Text(,Image)?|Image(,Text)?)\b'; // MMS is either "Image", "Text", or "Image,Text" (or Text,Image in this case)
	protected $V_FEAT= '\[(.*?)\]';	 // No count pattern... so, we'll have to rely upon spaces and order.
	protected $V_LOC= '\[(\d{5}.*?)\]';	 // No count pattern... so, we'll have to rely upon spaces and order.
	protected $V_BYTES= '(?<=\s)(\d{1,10})(?=\s)';	// Shouldn't return Item ID nor phone since that has no space in front of it (won't return phone #s)
	protected $V_BU= ['V_BYTES'];	// No count pattern... so, we'll have to rely upon spaces and order.
	protected $V_BD= ['V_BYTES'];	// No count pattern... so, we'll have to rely upon spaces and order.
	protected $V_NETWORK= false;	// This is a lower-cse string which comes between its preceeding and following columns... Not a great pattern, so we skip it
	protected $V_CAPS= '[A-Z]+\s'; // All caps A-Z string.
	protected $V_MAKE= ['V_CAPS'];
	protected $V_MODEL= '(?<=[A-Z]\s)(.*)(?=\[)'; // Just get whatever string comes after make and before location
	protected $V_DESC= ['V_CAPS']; // These seem to be the same but that's tough to tell AND IMEI could steal this.
	
    public $talk = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_SEIZURE','V_ET','V_ORIGIN','V_TERM','V_FORWARDING','V_IMEI','V_IMSI','V_CT','V_FEAT','V_LOC'];
    public $talk_no_loc = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_SEIZURE','V_ET','V_ORIGIN','V_TERM','V_FORWARDING','V_IMEI','V_IMSI','V_CT','V_FEAT','V_LOC'];
    public $data = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_ORIGIN','V_ET','V_BU','V_BD','V_NETWORK','V_IMEI','V_IMSI','V_MAKE','V_MODEL','V_LOC'];
    public $data_no_loc = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_ORIGIN','V_ET','V_BU','V_BD','V_NETWORK','V_IMEI','V_IMSI','V_MAKE','V_MODEL','V_LOC'];
    public $text = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_ORIGIN','V_TERM','V_IMEI','V_IMSI','V_DESC','V_MMS','V_FEAT','V_LOC'];
    public $text_no_loc = ['V_ITEM','V_CONN_DATE','V_CONN_TIME','V_ORIGIN','V_TERM','V_IMEI','V_IMSI','V_DESC','V_MMS','V_FEAT','V_LOC'];
	
	public $counter = ['V_HHMM'=>1];	// The Conn Time takes the first spot
	
	public $loc_link = [];

    public $item="",$conn_date="",$conn_time="",$seizure="",$et="",$origin="",$term="",$term_text="",$origin_text="",$forwarding="",$imei="",$imsi="",$ct="",$feat="",$mms="",$loc="",$network="",$make="",$model="",$desc="";
	public $bu="0",$bd="0";
	
	public $type="",$origin_p="",$term_p="",$origin_text_p="",$term_text_p="",$forwarding_p="",$loc_p="",$loc_end_p="",$latlng="",$et_p="",$duration_p="", $timezone="", $start_p="", $end_p="",$time_p="",$tower_count="0",$latlngFinal="",$dir="",$dirFinal="",$bu_orig="",$bd_orig="",$parent_number="",$distance="",$highlight="white;display:none";
	
	public $enable_shortcode_protection = true;
    /**
     * Create a new  instance.
     *
     * @return void
     */
    public function __construct($string, $type, $item = null, $doc_type = 'pdf')
    {
		if($string == 'warning'){
			$this->conn_date = 'Data Unavailable';
			return true;
		}
        $this->type = $type;
		// We'll use the hard-coded order from the above constants to walk through the string but we won't rely strictly upon it for order.
		// The order definitely helps, though. Without it, we'd be doing some guessing, so although it isn't strict, it should be adhered to.
		foreach($this->$type as $const){
			$results = [];
			$objp = str_replace('v_','',strtolower($const));
			if($this->$const===false){
				continue;
			}
			if(is_array($this->$const)){
				$v_pattern = $t_pattern = $this->$const[0];
				if($v_pattern==="V_PHONE"&&(strpos($type,'text') !== false || strpos($type,'talk') !== false)){
					$v_pattern="V_EXTENDEDPHONE";
				}
				$p_pattern = $this->$v_pattern;
				if(preg_match_all('/'.$p_pattern.'/',$string,$results)){
					if(!isset($this->counter[$t_pattern])){
						$this->counter[$t_pattern] = 0;
					}
					$results[0] = array_values(array_filter($results[0], function($value) { return !is_null($value) && $value !== ''; }));
					if($v_pattern==="V_EXTENDEDPHONE"){
						$this->$objp = '* Missing Orig or Term *';
					}
					if(isset($results[0][$this->counter[$t_pattern]]) && trim($results[0][$this->counter[$t_pattern]]) !== ''){
						// Shortcodes can be confused with item # especially when there's a wrapped item #
						// Problem is, we can't guarantee the wrapped item # digits will be in the beginning or end of a row...
						// and we can't know if it's going to match just 1 or 2 or 3 shortcodes
						if(!empty($this->item) && $v_pattern == "V_EXTENDEDPHONE" && $this->enable_shortcode_protection === true){
							$match_length = strlen(trim($results[0][$this->counter[$t_pattern]]));
							if(substr($this->item, (-1*$match_length)) == trim($results[0][$this->counter[$t_pattern]])){
								$this->counter[$t_pattern]++;
							}
						}
						if(isset($results[0][$this->counter[$t_pattern]])){
							$this->$objp = trim($results[0][$this->counter[$t_pattern]]);
						} else {
							$this->$objp = 'ERROR';
						}
						$this->counter[$t_pattern]++;
					}
					if($v_pattern==="V_HHMM"){
						$this->$objp = str_replace(' ','',$this->$objp);
					}
				}
				if($v_pattern==="V_PHONE"||$v_pattern==="V_EXTENDEDPHONE"){
					if(trim($this->$objp)=='911'){
						$this->highlight = '#D4FFEC';
					}
					Phone::firstOrCreate(['phone'=>$this->$objp]);
				}
			} else {					
				if(preg_match('/'.$this->$const.'/',$string,$results) && isset($results[0])){
					$results = array_values(array_filter($results, function($value) { return !is_null($value) && $value !== ''; }));
					$this->$objp = trim($results[0]);
				}
			}
			// There are some files which are 6+ digits, which is causing numbers to wrap to the next row, thus breaking.
			if($objp=='item' && $item !== null){
				$this->$objp = $item+1;
			}
			if($objp==="loc"){
				$this->$objp = str_replace(' ','',$this->$objp);
			}
			if($doc_type==='pdf'){
				if($objp=="bu"||$objp=="bd"){
					$digits = strlen($this->$objp);
					if($digits >=10){
						$this->$objp = number_format($this->$objp / 1000000000,1)."G";
					} else if($digits >=7){
						$this->$objp = number_format($this->$objp / 1000000,1)."M";
					} else if($digits >=4){
						$this->$objp = number_format($this->$objp / 1000,1)."K";
					} else {
						$this->$objp .= "B";
					}
				}
			}
			$objp_orig = $objp.'_orig';
			if(isset($this->$objp_orig)){
				$this->$objp_orig = $this->$objp;
			}
		}
		if($this->feat===$this->loc){
			$this->feat='';
		}
	}

    public function calcFields(){
        $this->calcTime();
        $this->calcOrigin();
        $this->calcTerm();
		$this->calcForwarding();
        $this->calcLatLngFinal();		
        $this->calcET();
        $this->calcDuration();
        $this->calcTowers();
    }

    public function calcDuration(){
        if($this->type!="text" && $this->type!="text_no_loc" && !empty($this->et)){
			$temp2 = [];
			$temp = explode(":",$this->et);
			if(!empty($this->seizure)){
				$temp2 = explode(":",$this->seizure);
			}
            $min=0;
            $sec=0;
            $hour=0;
            if(sizeof($temp2)>1){
                $min = $temp[0] + $temp2[0];
                $sec = $temp[1] + $temp2[1];
				if($sec>=60){
					$min+=1;
					$sec-=60;
				}
            }
            else{
                $min=ltrim($temp[0], '0');
                $min=$min==''?'0':$min;
                $sec=ltrim($temp[1], '0');
                $sec=$sec==''?'0':$sec;
				if($sec>=60){
					$min+=1;
					$sec-=60;
				}
            }
            if($min>=60){
                $hour += intdiv($min,60);
                $min = $min % 60;
            }
            if($min || $hour){
                if($hour == 0){
                    $this->duration_p = $min."m, ".$sec."s";
                }
                else{
                    $this->duration_p = $hour."h, ".$min."m, ".$sec."s";
                }
            }
            else{
                $this->duration_p = $sec."s";
            }
        }
    }

    public function calcET(){
        if($this->type!="text" && $this->type!="text_no_loc"){
            $temp = explode(":",$this->et);
            if(sizeof($temp)>1){
                $this->et_p = "$temp[0]:$temp[1]";
            }
            else{
                $this->et_p = "";
            }
        }
    }


    public function calcTime(){
        $this->time_p = $this->conn_date." ".$this->conn_time;
		$this->time_mdyh = $this->conn_date." ".substr(trim($this->conn_time),0,2);
    }
	
	public function formatNumber($number,$offset=0){
		$first = substr($number, 0, 1);
		$first_two = substr($number, 0, 2);
		if($number==='-1'||strlen($number)<10){
			return '';
		}
		if($first==='0'||$first_two==='10'||$first_two==='11'||strlen($number)>11){
			return $number;
		}
		return substr_replace(substr_replace(substr_replace(substr($number,$offset), '-', 6, 0),') ',3,0),'(',0,0);
	}

    public function calcOrigin(){
        if(strlen($this->origin)===10){
			$this->origin_p = self::formatNumber($this->origin,0);
		} else if(strlen($this->origin)>10&&strlen($this->origin)<=15){
            $this->origin_p = self::formatNumber($this->origin,1);
            $this->origin_p = substr($this->origin_p,0,14);
        }
        else{
            $this->origin_p = $this->origin;
        }
    }

    public function calcTerm(){
        if(strlen($this->term)===10){
			$this->term_p = self::formatNumber($this->term,0);
		} elseif(strlen($this->term)>10&&strlen($this->term)<=15){
            $this->term_p = self::formatNumber($this->term,1);
            $this->term_p = substr($this->term_p,0,14);
        } else{
            $this->term_p = $this->term;
        }
    }

    public function calcForwarding(){
		$this->forwarding_p = '';
		if(empty($this->forwarding)){
			$this->forwarding_p = '';
		} else {
			if(strlen($this->forwarding)===10){
				$this->forwarding_p = self::formatNumber($this->forwarding,0);
			} elseif(strlen($this->forwarding)>=10&&strlen($this->forwarding)<=15){
				$this->forwarding_p = self::formatNumber($this->forwarding,1);
				$this->forwarding_p = trim(substr($this->forwarding_p,0,18));
			} elseif(strlen($this->forwarding)>=7&&strlen($this->forwarding)<=15) {
				$this->forwarding_p = substr_replace($this->forwarding, '-', 3, 0);
			} else {
				$this->forwarding_p = $this->forwarding;
			}
		}
    }

    public function calcTowers(){
		if(is_string($this->loc)){
			if(false == strpos($this->loc,',')){
				$arr_temp = explode(";",$this->loc);
			} else {
				$arr_temp = explode(",",$this->loc);
			}
		} else {
			$arr_temp = is_array($this->loc)? $this->loc : (array)$this->loc;
		}
        
        $gps_temp = array_filter($arr_temp, function($value) {
							return ($value !== null && $value !== false); 
						});
        if(count($gps_temp)>1){
            $this->tower_count = count($arr_temp);
        } else{
            $this->tower_count = "";
        }
		if(!empty($arr_temp)){
			$host = 'https://mapware.net/denis/api_fn1.html';
			$params = [];
			$coordarray = [];
			for($i=0; $i < count($arr_temp); $i++){		
				$geo_temp = array_filter(explode(":",$arr_temp[$i]), function($value) {
						return ($value !== null && $value !== false); 
					});
					if($this->item == 5){
						Log::debug(json_encode($geo_temp));
					}
				if(!empty($geo_temp)){
					$ltp = $lnp = $drp = $latlng = null;
					$azimuth = '-1';
					$label = 'Tower';
					if(count($geo_temp) >= 4 && count($geo_temp) <= 5){
						$ltp = 2;
						$lnp = 1;
						$drp = 3;
					} else if(count($geo_temp) >= 6){
						$ltp = 3;
						$lnp = 2;
						$drp = 4;
					}
					if(isset($geo_temp[$ltp]) && $geo_temp[$ltp] !== '' && isset($geo_temp[$lnp]) && $geo_temp[$lnp] !== ''){
						$latlng = $geo_temp[$ltp].','.$geo_temp[$lnp];
						if($i==(count($arr_temp)-1)){
							$this->latlngFinalTemp = $latlng;
							$this->latlngFinalTempShort = substr($geo_temp[$ltp],0,-1).','.substr($geo_temp[$lnp],0,-1);
						} else if($i == 0){
							$this->latlngFirstTemp = $latlng;
							$this->latlngFirstTempShort = substr($geo_temp[$ltp],0,-1).','.substr($geo_temp[$lnp],0,-1);
						}
						if(array_key_exists($drp,$geo_temp)){
							$azimuth = ($geo_temp[$drp] !== '')? $geo_temp[$drp] : '-1';
						}
					}
					if(!empty($latlng)&&$azimuth!==''&&!empty($label)&&($i==0 || $i==(count($arr_temp)-1))){	// Only display the first and last towers
						if(!empty($this->tower_count) && $this->tower_count == 2 && isset($this->latlngFinalTemp) && isset($this->latlngFirstTemp)){
							if($this->latlngFinalTempShort == $this->latlngFirstTempShort){
								$this->tower_count = $this->tower_count -1;
							}
						}
						$coordstring = $latlng;
						$coordstring .= ",".$label.",Blue,".$azimuth."|";
						$coordarray[] = $coordstring;
					}
				}
			}
			$params['towerRange'] = 2;//$this->tower_count;
			if(!empty($coordarray) && empty($this->loc_link)){
				foreach($coordarray as $coordstring){
					$params['points'] = rtrim($coordstring,'|');
					if(!empty($params['points'])){
						$paramstring = '';
						foreach($params as $k=>$p){
							$paramstring .= $k.'='.$p.'&';
						}
						$this->loc_link[] = $host.'?'.rtrim($paramstring,'&');
					}
				}
			}
			if($this->tower_count > 1 && $this->highlight !== '#D4FFEC'){
				$this->highlight = "#DAF4FA";
			}
		}
		return true;
    }
	
    public function calcDir($dir){
		$cardinals = ['(N)','(NNE)','(NE)','(ENE)','(E)','(ESE)','(SE)','(SSE)','(S)','(SSW)','(SW)','(WSW)','(W)','(WNW)','(NW)','(NNW)'];
		$diameter = 360/count($cardinals);
		$radius = $diameter/2;
		if($dir>=(360-$radius)){
			return $cardinals[0];
		} else {
			for($i = 0; $i < count($cardinals); $i++){
				if($dir>=((2*$i-1)*$radius) && $dir<(((2*$i-1)*$radius)+$diameter)){
					return $cardinals[$i];
				}
			}
		}
	}

	/**
		Calculates the start and end location
	**/
    public function calcLatLngFinal($geo_temp = null){
	    if(empty($this->loc)){
			return false;
		}
		if(!empty($geo_temp)){
			$exploded = [trim($geo_temp)];
			$latlng = 'latlngFinal';
			$dirvar = 'dirFinal';
        } else {
			if(is_string($this->loc)){
				if(false == strpos($this->loc,',')){
					$exploded = explode(";",$this->loc);
				} else {
					$exploded = explode(",",$this->loc);
				}
			} else {
				$exploded = is_array($this->loc)? $this->loc : (array)$this->loc;
			}
			$geo_temp = trim($exploded[0]);
            $latlng = 'latlng';
            $dirvar = 'dir';
		}
		$this->$latlng = null;
		$this->$dirvar = "";
		$arr_temp = array_filter(explode(":",$geo_temp), function($value) {
				return ($value !== null && $value !== false); 
			});

		$sub_key = array_search($arr_temp,$exploded);		
		$ltp = $lnp = $drp = null;
		
		if(count($arr_temp) >= 4 && count($arr_temp) <= 5){
			$ltp = 2;
			$lnp = 1;
			$drp = 3;
		} else if(count($arr_temp) >= 6){
			$ltp = 3;
			$lnp = 2;
			$drp = 4;
		}
		
		if($ltp!=null && isset($arr_temp[$ltp]) && $arr_temp[$ltp] !== '' && isset($arr_temp[$lnp]) && $arr_temp[$lnp] !== ''){
			if(!array_key_exists($drp,$arr_temp)){
				$arr_temp[$drp] = '-1';
			}
			$this->$dirvar = $this->dirFinal = $this->calcDir($arr_temp[$drp]);
			$this->$latlng = "$arr_temp[$ltp],$arr_temp[$lnp]";
		} else {			
			$this->$latlng = "0.0,0.0";
		}
		
		if(count($exploded)>1){	// This means there is a different end point than start point, so we have to recalculate it.
			self::calcLatLngFinal(end($exploded));
		}
		
		if(empty($this->latlngFinal)){
			$this->latlngFinal = $this->latlng;
		}

		return true;
    }
	
	public function getConnDate($timezone,$noadjust=false){
        $tz = $this->timezone!=""?$this->timezone:$timezone;
		$tz = Helper::offsetUTC($tz);
		$tempdate = new \DateTime($this->time_p, new \DateTimeZone('UTC'));
		if(!$noadjust){
			$tempdate->setTimezone(new \DateTimeZone($tz));
		}
		$this->conn_date=$tempdate->format('m/d/y');
        return $this->conn_date;
	}

    public function getStartP($timezone,$noadjust=false){
        $tz = $this->timezone!=""?$this->timezone:$timezone;
		$tz = Helper::offsetUTC($tz);
        //if($this->start_p==""){
            $tempdate = new \DateTime($this->time_p, new \DateTimeZone('UTC'));
			if(!$noadjust){
				$tempdate->setTimezone(new \DateTimeZone($tz));
			}
            $this->start_p=$tempdate->format('g:i:s A');
        //}
        return $this->start_p;
    }

    public function getEndP($timezone,$noadjust=false){
        $tz = $this->timezone!=""?$this->timezone:$timezone;
		$tz = Helper::offsetUTC($tz);
        $tempdate = new \DateTime($this->time_p, new \DateTimeZone('UTC'));
		if(!$noadjust){
			$tempdate->setTimezone(new \DateTimeZone($tz));
		}
        if($this->type!="text" && $this->type!="text_no_loc" && !empty($this->et)){
            $temp = explode(":",$this->et);
			$temp2 = [];
			if(!empty($this->seizure)){
				$temp2 = explode(":",$this->seizure);
			}
            if(sizeof($temp2)>1){
                $min = $temp[0] + $temp2[0];
                $sec = $temp[1] + $temp2[1];
                if($sec>=60){
                    $min += intdiv($sec,60);
                    $sec = $sec % 60;
                }
            }
            else{
                $min = $temp[0];
                $sec = $temp[1];
                if($sec>=60){
                    $min += intdiv($sec,60);
                    $sec = $sec % 60;
                }
            }
            $dv = new \DateInterval("PT".$min."M".$sec."S");
            $tempdate->add($dv);

        }
        $this->end_p = $tempdate->format('g:i:s A');
        return $this->end_p;
    }

}

