<?php

namespace App\Http\Controllers;
use App\Phone as Phone;
use App\VerizonTowers as VerizonTowers;
use App\Http\Controllers\AttRowEntry as AttRowEntry;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Helper as Helper;

class VerizonRowEntryCSV extends AttRowEntry{
	
	public $highlight="white;display:none";
	public $towers = true;
	public $region = 'America';
	public $city = 'Chicago';
    /**
     * Create a new  instance.
     *
     * @return void
     */
    public function __construct($row,$data,$type,$headers)
    {
		$this->original_timezone = 'UTC';
		if(isset($headers['Start Date/Time']) && isset($data[$headers['Start Date/Time']])){
			preg_match('#\((.*?)\)#', $data[$headers['Start Date/Time']], $match);
			if(isset($match[1])){
				$data[$headers['Start Date/Time']] = str_replace([$match[1],'(',')'],'',$data[$headers['Start Date/Time']]);
			}
			$orig_date_time = array_filter(explode(' ',$data[$headers['Start Date/Time']]));
			if(isset($match[1])){
				$this->timezone = trim(str_replace(['GMT','(GMT)'],'',$match[1]));
			}
		}
		if(isset($headers['Record Open Date/Time']) && isset($data[$headers['Record Open Date/Time']])){
			preg_match('#\((.*?)\)#', $data[$headers['Record Open Date/Time']], $match);
			if(isset($match[1])){
				$data[$headers['Record Open Date/Time']] = str_replace([$match[1],'(',')'],'',$data[$headers['Record Open Date/Time']]);
			}
			$orig_date_time = array_filter(explode(' ',$data[$headers['Record Open Date/Time']]));
			if(isset($match[1])){
				$this->timezone = trim(str_replace(['GMT','(GMT)'],'',$match[1]));
			}
		}
		if(!empty($this->timezone)){
			$this->timezone = Helper::offsetUTCReverse($this->timezone);
			if($this->timezone != 'UTC'){
				$this->original_timezone = 'CORRECT';
			}
		}
		$call_duration = 0;
		$this->seizure = null;
		if(isset($headers['Stop Date/Time']) && isset($data[$headers['Stop Date/Time']])){
			preg_match('#\((.*?)\)#', $data[$headers['Stop Date/Time']], $match);
			if(isset($match[1])){
				$data[$headers['Stop Date/Time']] = str_replace([$match[1],'(',')'],'',$data[$headers['Stop Date/Time']]);
			}
			$term_date_time = array_filter(explode(' ',$data[$headers['Stop Date/Time']]));
			//$this->seizure = $data[$headers['Stop Date/Time']];
			if(!empty($data[$headers['Start Date/Time']])){
				$call_duration = strtotime($data[$headers['Stop Date/Time']]) - strtotime($data[$headers['Start Date/Time']]);
			}
		}
		$this->type = $type;
		$this->item = $row-1;
		$this->conn_date = (isset($orig_date_time)&&!empty($orig_date_time[0]))? $orig_date_time[0] : date("m-d-Y");
		$this->conn_time = (isset($orig_date_time)&&isset($orig_date_time[1]))? $orig_date_time[1] : '00:00:00';
		$this->origin = (isset($headers['MSISDN'])&&isset($data[$headers['MSISDN']]))? self::cleannumber($data[$headers['MSISDN']]) : '';
		$this->term = (isset($headers['Called #'])&&isset($data[$headers['Called #']]))? self::cleannumber($data[$headers['Called #']]) : '';
		$this->ct = (isset($headers['Dir'])&&isset($data[$headers['Dir']]))? $data[$headers['Dir']] : '';
		$this->imsi = $this->imei = $this->desc = $this->make = $this->model = $this->feat = '';
		$this->azimuth = (isset($headers['Cell Face'])&&isset($data[$headers['Cell Face']]))? $data[$headers['Cell Face']] : '';
		$call_length = ['minutes'=>0];
		$call_length['seconds'] = $call_duration;
		if(intval($call_length['seconds'])>59){	
			$call_length['minutes_total'] = $call_length['seconds']/60;
			$call_length['minutes'] = intval(floor($call_length['minutes_total']));
			$fraction = $call_length['minutes_total'] - $call_length['minutes'];
			unset($call_length['minutes_total']);
			$call_length['seconds'] = intval(floor(60*$fraction));
			if($call_length['minutes']>59){
				$call_length['hours_total'] = $call_length['minutes']/60;
				$call_length['hours'] = intval(floor($call_length['hours_total']));
				$fraction = $call_length['hours_total'] - $call_length['hours'];
				unset($call_length['hours_total']);
				$call_length['minutes'] = intval(floor(60*$fraction));
			}
		}
		if($call_length['seconds']<=9 && strlen($call_length['seconds'])){
			$call_length['seconds'] = '0'.$call_length['seconds'];
		}
		ksort($call_length);
		$this->et = implode(':',$call_length);
		
		$towers = [];
		
		if(isset($headers['Cell ID'])&&!empty($data[$headers['Cell ID']])){
			$id = ltrim($data[$headers['Cell ID']],$this->azimuth);
			$id = !empty($id)? $id : 0;
			$towers[] = ['id'=>$id,'sector'=>$this->azimuth];
		}
		
		$this->loc = '[';
		if(!empty($towers)){
			$this->tower_count = count(array_unique(array_column($towers,'id')));
			$host = 'https://mapware.net/denis/api_fn1.html';
			$params = [];
			$coordstring = "";
			for($i=0; $i < count($towers); $i++){
				$tower = $towers[$i];
				$obj = VerizonTowers::where('id',$tower['id'])->where('sector',$tower['sector'])->first();
				$label = 'Tower';
				if(!empty($obj)){
					if(!isset($obj->azimuth)||empty($obj->azimuth)){
						$obj->azimuth = '-1';
					}
					switch(trim($tower['sector'])){
						case '1':
							$tower['sector'] = 0;
							break;
						case '2':
							$tower['sector'] = 180;
							break;
						case '3':
							$tower['sector'] = 270;
							break;
						default:
							$tower['sector'] = 90;
							break;								
					}
					$this->loc .= str_replace(['[',']'],'',$obj->neid).':'.$obj->azimuth.':'.$obj->longitude.':'.$obj->latitude.':'.str_replace(['[',']'],'',$tower['sector']).':,';
					if($i==0 || $i==(count($towers)-1)){
						$coordstring .= ($obj->latitude).','.($obj->longitude);
						$coordstring .= ",".$label.",Blue,".$obj->azimuth."|";
					}
				} else {
					// If we do not have that tower in the database, we need to return a failure message and request file upload
					// REFACTOR: Since we do not have the Verizon tower data yet we will just skip this.
					/**
						$this->towers = false;
						return false;
					**/
				}
			}
			if($this->tower_count > 1){
				$this->highlight = "#DAF4FA";
			}
			$params['towerRange'] = 2;//$this->tower_count;
			if(!empty($coordstring)){
				$params['points'] = rtrim($coordstring,'|');
			}
			if(isset($params['points'])){
				$paramstring = '';
				foreach($params as $k=>$p){
					$paramstring .= $k.'='.$p.'&';
				}
				$this->loc_link[] = $host.'?'.rtrim($paramstring,'&');
			}	
		}
		$this->loc = rtrim($this->loc,',');
		$this->loc .= ']';
		if(!empty($headers['Orig Total MB'])&&!empty($data[$headers['Orig Total MB']])){
			$this->bu = 0;
			$this->bd = !empty($headers['Orig Total MB'])&&!empty($data[$headers['Orig Total MB']])? $data[$headers['Orig Total MB']] : 0;
			$this->bu_orig = $this->bu;
			$this->bd = intval($this->bd * 1000000);
			$this->bd_orig = $this->bd;
			
			if($this->bd > 0){
				if($this->bu >= 1000000000){
					$this->bu = number_format($this->bu / 1000000000,1)."G";
				} else if($this->bu >=1000000){
					$this->bu = number_format($this->bu / 1000000,1)."M";
				} else if($this->bu >=1000){
					$this->bu = number_format($this->bu / 1000,1)."K";
				} else {
					$this->bu .= "B";
				}
				if($this->bd >= 1000000000){
					$this->bd = number_format($this->bd / 1000000000,1)."G";
				} else if($this->bd >=1000000){
					$this->bd = number_format($this->bd / 1000000,1)."M";
				} else if($this->bd >=1000){
					$this->bd = number_format($this->bd / 1000,1)."K";
				} else {
					$this->bd .= "B";
				}
			}
		}
		Phone::firstOrCreate(['phone'=>$this->origin]);
		Phone::firstOrCreate(['phone'=>$this->term]);		
	}

	public function cleannumber($number){
			$number = str_replace("[<=9999999]",'',$number);
			return str_replace('-','',$number);
	}
		
}

