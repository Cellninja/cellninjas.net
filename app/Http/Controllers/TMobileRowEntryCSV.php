<?php

namespace App\Http\Controllers;
use App\Phone as Phone;
use App\SprintTowers as SprintTowers;
use App\Http\Controllers\AttRowEntry as AttRowEntry;
use App\Http\Controllers\Helper as Helper;
use Illuminate\Support\Facades\Log;

class TMobileRowEntryCSV extends AttRowEntry{
	
	public $highlight="white;display:none";
	public $towers = true;
    /**
     * Create a new  instance.
     *
     * @return void
     */
    public function __construct($row,$data,$type,$headers)
    {
		$this->type = $type;
		$this->item = $row-1;
		$this->conn_date = !empty($data[$headers['Date']])? $data[$headers['Date']] : date("Y-m-d");
		$this->conn_time = !empty($data[$headers['Time']])? $data[$headers['Time']] : 0;
		$this->origin = !empty($data[$headers['Calling Number']])? $data[$headers['Calling Number']] : 0;
		$this->term = !empty($data[$headers['Dialed Number']])? $data[$headers['Dialed Number']] : (isset($data[$headers['Called Number']])? $data[$headers['Called Number']] : '');
		$this->addy_start = !empty($headers['1st Tower Address'])&&isset($data[$headers['1st Tower Address']])? $data[$headers['1st Tower Address']] : '';
		$this->addy_end = !empty($headers['Last Tower Address'])&&isset($data[$headers['Last Tower Address']])? $data[$headers['Last Tower Address']] : $this->addy_start;
		$this->city_start = !empty($headers['1st Tower City'])&&isset($data[$headers['1st Tower City']])? $data[$headers['1st Tower City']] : '';
		$this->city_end = !empty($headers['Last Tower City'])&&isset($data[$headers['Last Tower City']])? $data[$headers['Last Tower City']] : $this->city_start;
		$this->state_start = !empty($headers['1st Tower State'])&&isset($data[$headers['1st Tower State']])? $data[$headers['1st Tower State']] : '';
		$this->state_end = !empty($headers['Last Tower State'])&&isset($data[$headers['Last Tower State']])? $data[$headers['Last Tower State']] : $this->state_start;
		$this->zip_start = !empty($headers['1st Tower Zip'])&&isset($data[$headers['1st Tower Zip']])? $data[$headers['1st Tower Zip']] : '';
		$this->zip_end = !empty($headers['Last Tower Zip'])&&isset($data[$headers['Last Tower Zip']])? $data[$headers['Last Tower Zip']] : $this->zip_start;
		$this->service_code = !empty($headers['Service Code'])&&isset($data[$headers['Service Code']])? $data[$headers['Service Code']] : '';
		$this->imsi = !empty($headers['IMSI'])&&isset($data[$headers['IMSI']])? $data[$headers['IMSI']] : '';
		$this->imei = !empty($headers['IMEI'])&&isset($data[$headers['IMEI']])? $data[$headers['IMEI']] : '';
		$this->desc = !empty($headers['Desc'])&&isset($data[$headers['Desc']])? $data[$headers['Desc']] : ''; // This doesn't exist on TMobile
		$this->make = !empty($headers['Make'])&&isset($data[$headers['Make']])? $data[$headers['Make']] : ''; // This doesn't exist on TMobile
		$this->model = !empty($headers['Model'])&&isset($data[$headers['Model']])? $data[$headers['Model']] : ''; // This doesn't exist on TMobile
		$this->ct = !empty($headers['Ct'])&&isset($data[$headers['Ct']])? $data[$headers['Ct']] : ''; // This doesn't exist on TMobile
		$this->feat = !empty($headers['Feature'])&&isset($data[$headers['Feature']])? $data[$headers['Feature']] : ''; // This doesn't exist on TMobile
		$this->azimuth = !empty($headers['1st Tower Azimuth'])&&isset($data[$headers['1st Tower Azimuth']])? $data[$headers['1st Tower Azimuth']] : '';
		$call_length = ['minutes'=>0];
		$call_length['seconds'] = $duration = !empty($headers['Duration'])&&isset($data[$headers['Duration']])? intval($data[$headers['Duration']]) : 00;
		if(intval($call_length['seconds'])>59){	
			$call_length['minutes_total'] = $call_length['seconds']/60;
			$call_length['minutes'] = intval(floor($call_length['minutes_total']));
			$fraction = $call_length['minutes_total'] - $call_length['minutes'];
			unset($call_length['minutes_total']);
			$call_length['seconds'] = intval(floor(60*$fraction));
			if($call_length['minutes']>59){
				$call_length['hours_total'] = $call_length['minutes']/60;
				$call_length['hours'] = intval(floor($call_length['hours_total']));
				$fraction = $call_length['hours_total'] - $call_length['hours'];
				unset($call_length['hours_total']);
				$call_length['minutes'] = intval(floor(60*$fraction));
			}
		}
		if($call_length['seconds']<=9 && strlen($call_length['seconds'])){
			$call_length['seconds'] = '0'.$call_length['seconds'];
		}
		ksort($call_length);
		$this->seizure = '0:00';
		$this->et = implode(':',$call_length);
		$towers = [];
		
		if(!empty($headers['1st Tower LONG'])&&!empty($data[$headers['1st Tower LONG']]) &&!empty($headers['1st Tower LAT'])&&!empty($data[$headers['1st Tower LAT']])){
			$sector = !empty($headers['1st LTE Sector ID'])&&isset($data[$headers['1st LTE Sector ID']])? $data[$headers['1st LTE Sector ID']] : '';
			$id = (!empty($headers['1st Cell ID'])&&!empty($data[$headers['1st Cell ID']]))? $data[$headers['1st Cell ID']] : ((!empty($headers['1st LTE Site ID'])&&!empty($data[$headers['1st LTE Site ID']])) ? $data[$headers['1st LTE Site ID']] : 0);
			$long = $data[$headers['1st Tower LONG']];
			// If the parsed coordinate is incomplete (ie. truncated by the ".", we have to go by address
			if(strlen($long) < 6 && $this->addy_start == $this->addy_end && isset($data[$headers['Last Tower LONG']])){
				$long = $data[$headers['Last Tower LONG']];
			}
			$lat = $data[$headers['1st Tower LAT']];
			if(strlen($lat) < 6 && $this->addy_start == $this->addy_end && isset($data[$headers['Last Tower LAT']])){
				$lat = $data[$headers['Last Tower LAT']];
			}
			$azimuth = !empty($headers['1st Tower Azimuth'])&&isset($data[$headers['1st Tower Azimuth']])? $data[$headers['1st Tower Azimuth']] : '-1';
			$towers[] = ['id'=>$id,'sector'=>$sector,'latitude'=>$lat,'longitude'=>$long,'azimuth'=>$azimuth];
		}
		if(!empty($headers['Last Tower LONG'])&&!empty($data[$headers['Last Tower LONG']]) &&!empty($headers['Last Tower LAT'])&&!empty($data[$headers['Last Tower LAT']])){
			$sector = !empty($headers['Last LTE Sector ID'])&&isset($data[$headers['Last LTE Sector ID']])? $data[$headers['Last LTE Sector ID']] : '';
			$id = (!empty($headers['Last Cell ID'])&&!empty($data[$headers['Last Cell ID']]))? $data[$headers['Last Cell ID']] : ((!empty($headers['Last LTE Site ID'])&&!empty($data[$headers['Last LTE Site ID']])) ? $data[$headers['Last LTE Site ID']] : 0);
			$long = $data[$headers['Last Tower LONG']];
			$lat = $data[$headers['Last Tower LAT']];
			// If the parsed coordinate is incomplete (ie. truncated by the ".", we have to go by address
			if(strlen($long) < 6 && $this->addy_start == $this->addy_end && isset($data[$headers['1st Tower LONG']])){
				$long = $data[$headers['1st Tower LONG']];
			}
			if(strlen($lat) < 6 && $this->addy_start == $this->addy_end && isset($data[$headers['1st Tower LAT']])){
				$lat = $data[$headers['1st Tower LAT']];
			}
			$azimuth = !empty($headers['Last Tower Azimuth'])&&isset($data[$headers['Last Tower Azimuth']])? $data[$headers['Last Tower Azimuth']] : '-1';
			$towers[] = ['id'=>$id,'sector'=>$sector,'latitude'=>$lat,'longitude'=>$long,'azimuth'=>$azimuth];
		}
		// Call has no duration
		if(empty($duration)){
			if(!empty($headers['Completion Code'])){
				$data[$headers['Completion Code']] = 'Failed';
			}
		} else {
			/*
			These record types do have a duration: 'mSOriginatingSMSinMSC' & 'mSTerminatingSMSinMSC'
			if($this->type == 'text'){
				$this->type = 'talk';
			}
			*/
		}
		$this->forwarding = null;
		if(!empty($headers['Call Type']) && false !== strpos($data[$headers['Call Type']],'callForwarding')){
			// Forwarding
			$this->type = 'talk';
			$this->forwarding = (!empty($data[$headers['Called Number']])? $data[$headers['Called Number']] : (isset($data[$headers['Dialed Number']])? $data[$headers['Dialed Number']] : '')).'(F)';
		}
		if(!empty($this->service_code) && in_array($this->service_code,['20','21','28','29','2A','2B','31','02A','02A;11','029;11','021;11','041;2A;11'])){
			// Forwarding
			$this->type = 'talk';
			$acronym = '(F)';
			if($this->service_code == '021;11' || $this->service_code == '041;2A;11'){
				$acronym = '(FVM)';
			}
			$this->forwarding = (!empty($data[$headers['Called Number']])? $data[$headers['Called Number']] : (isset($data[$headers['Dialed Number']])? $data[$headers['Dialed Number']] : '')).$acronym;
		}
		
		if(!empty($headers['Completion Code']) && $data[$headers['Completion Code']] == 'Failed'){
			$this->term = !empty($data[$headers['Called Number']])? $data[$headers['Called Number']] : (isset($data[$headers['Dialed Number']])? $data[$headers['Dialed Number']] : '');
		}		
		$this->loc = '[';
		if(!empty($towers)){
			$this->tower_count = count(array_unique(array_column($towers,'id')));
			$host = 'https://mapware.net/denis/api_fn1.html';
			$params = [];
			$coordstring = "";
			for($i=0; $i < count($towers); $i++){
				$tower = $towers[$i];
				$label = 'Tower';
				$this->loc .= $tower['id'].':'.$tower['sector'].':'.$tower['longitude'].':'.$tower['latitude'].':'.$tower['azimuth'].':'.$tower['sector'].',';
				if($i==0 || $i==(count($towers)-1)){
					$coordstring .= ($tower['latitude']).','.($tower['longitude']);
					$coordstring .= ",".$label.",Blue,".$tower['azimuth']."|";
				}
			}
			if($this->tower_count > 1){
				$this->highlight = "#DAF4FA";
			}
			$params['towerRange'] = 2;//$this->tower_count;
				if(!empty($coordstring)){
					$params['points'] = rtrim($coordstring,'|');
				}
				if(isset($params['points'])){
					$paramstring = '';
					foreach($params as $k=>$p){
						$paramstring .= $k.'='.$p.'&';
					}
					$this->loc_link[] = $host.'?'.rtrim($paramstring,'&');
				}
		}
		if(trim($this->term)=='911'){
			$this->highlight = '#D4FFEC';
		}
		$this->loc = rtrim($this->loc,',');
		$this->loc .= ']';		

		$this->loc_end_p = (!empty($this->addy_end)&&!empty($this->city_end)&&!empty($this->state_end))? $this->addy_end.','.$this->city_end.','.$this->state_end : null;
		$this->loc_p = (!empty($this->addy_start)&&!empty($this->city_start)&&!empty($this->state_start))? $this->addy_start.','.$this->city_start.','.$this->state_start : null;
		
		if(($type=='text' && (strtotime('-24 months', time()) > strtotime($this->conn_date)))||$type=='mms'){
			$this->original_timezone = 'PST'; // REFACTOR: PDT needs to be accounted for
		}
		if($type=='voice' && (strtotime('-24 months', time()) > strtotime($this->conn_date))){
			$this->original_timezone = 'CORRECT';
		}
		
        if($type=="data"||$type=="data_no_loc"){ // BU/BD data does not exist on TMobile
			$this->bu = !empty($headers['Bytes Up'])&&!empty($data[$headers['Bytes Up']])? $data[$headers['Bytes Up']] : '';
			$this->bu_orig = $this->bu;
			$this->bd = !empty($headers['Bytes Down'])&&!empty($data[$headers['Bytes Down']])? $data[$headers['Bytes Down']] : '';
			$this->bd_orig = $this->bd;
			
				if($this->bu >= 1000000000){
					$this->bu = number_format($this->bu / 1000000000,1)."G";
				} else if($this->bu >=1000000){
					$this->bu = number_format($this->bu / 1000000,1)."M";
				} else if($this->bu >=1000){
					$this->bu = number_format($this->bu / 1000,1)."K";
				} else {
					$this->bu .= "B";
				}
				if($this->bd >= 1000000000){
					$this->bd = number_format($this->bd / 1000000000,1)."G";
				} else if($this->bd >=1000000){
					$this->bd = number_format($this->bd / 1000000,1)."M";
				} else if($this->bd >=1000){
					$this->bd = number_format($this->bd / 1000,1)."K";
				} else {
					$this->bd .= "B";
				}
        }
		Phone::firstOrCreate(['phone'=>$this->origin]);
		Phone::firstOrCreate(['phone'=>$this->term]);
    }


}

