<?php

namespace App\Http\Controllers;
use App\Phone as Phone;
use App\SprintTowers as SprintTowers;
use App\Http\Controllers\AttRowEntry as AttRowEntry;
use Illuminate\Support\Facades\Log;

class SprintRowEntryCSV extends AttRowEntry{
	
	public $highlight="white;display:none";
	public $towers = true;
	public $region = 'America';
	public $city = 'Chicago';
    /**
     * Create a new  instance.
     *
     * @return void
     */
    public function __construct($row,$data,$type,$headers)
    {
		if(isset($data[$headers['START_DATE']])){
			$orig_date_time = array_filter(explode(' ',$data[$headers['START_DATE']]));
		}
		if(isset($data[$headers['END_DATE']])){
			$term_date_time = array_filter(explode(' ',$data[$headers['END_DATE']]));
		}
		$this->type = $type;
		$this->item = $row-1;
		$this->conn_date = !empty($orig_date_time[0])? $orig_date_time[0] : date("m-d-Y");
		$this->conn_time = isset($orig_date_time[1])? $orig_date_time[1] : 0;
		$this->origin = isset($data[$headers['CALLING_NBR']])? self::cleannumber($data[$headers['CALLING_NBR']]) : '';
		$this->term = isset($data[$headers['DIALED_DIGITS']])? self::cleannumber($data[$headers['DIALED_DIGITS']]) : (isset($data[$headers['CALLED_NBR']])? self::cleannumber($data[$headers['CALLED_NBR']]) : '');
		$this->imsi = !empty($headers['IMSI'])&&isset($data[$headers['IMSI']])? $data[$headers['IMSI']] : '';
		$this->imei = !empty($headers['IMEI'])&&isset($data[$headers['IMEI']])? $data[$headers['IMEI']] : '';
		$this->desc = !empty($headers['DESC'])&&isset($data[$headers['DESC']])? $data[$headers['DESC']] : '';
		$this->make = !empty($headers['MAKE'])&&isset($data[$headers['MAKE']])? $data[$headers['MAKE']] : '';
		$this->model = !empty($headers['MODEL'])&&isset($data[$headers['MODEL']])? $data[$headers['MODEL']] : '';
		$this->ct = !empty($headers['CT'])&&isset($data[$headers['CT']])? $data[$headers['CT']] : '';
		$this->feat = !empty($headers['FEATURE'])&&isset($data[$headers['FEATURE']])? $data[$headers['FEATURE']] : '';
		$call_length = ['minutes'=>0];
		$call_length['seconds'] = $duration = $data[$headers['DURATION (SEC)']];
		if(intval($call_length['seconds'])>59){	
			$call_length['minutes_total'] = $call_length['seconds']/60;
			$call_length['minutes'] = intval(floor($call_length['minutes_total']));
			$fraction = $call_length['minutes_total'] - $call_length['minutes'];
			unset($call_length['minutes_total']);
			$call_length['seconds'] = intval(floor(60*$fraction));
			if($call_length['minutes']>59){
				$call_length['hours_total'] = $call_length['minutes']/60;
				$call_length['hours'] = intval(floor($call_length['hours_total']));
				$fraction = $call_length['hours_total'] - $call_length['hours'];
				unset($call_length['hours_total']);
				$call_length['minutes'] = intval(floor(60*$fraction));
			}
		}
		if($call_length['seconds']<=9 && strlen($call_length['seconds'])){
			$call_length['seconds'] = '0'.$call_length['seconds'];
		}
		ksort($call_length);
		$this->seizure = '0:00';
		$this->et = implode(':',$call_length);
		$towers = [];
		
		if(!empty($headers['1ST CELL'])&&!empty($data[$headers['1ST CELL']])){
			$sector = substr($data[$headers['1ST CELL']],0,1);
			$id = ltrim($data[$headers['1ST CELL']],$sector);
			$id = !empty($id)? $id : 0;
			$towers[] = ['id'=>$id,'sector'=>$sector];
		}
		if(!empty($headers['LAST CELL'])&&!empty($data[$headers['LAST CELL']])){
			$sector = substr($data[$headers['LAST CELL']],0,1);
			$id = ltrim($data[$headers['LAST CELL']],$sector);
			$id = !empty($id)? $id : 0;
			$towers[] = ['id'=>$id,'sector'=>$sector];
		}
		// Call has no duration
		if(empty($duration)){
			if(!empty($headers['NEID']) && ($data[$headers['NEID']] >= 200 && $data[$headers['NEID']] <= 280) || ($data[$headers['NEID']] >= 600 && $data[$headers['NEID']] <= 602)){
				if(empty($towers)){
					$this->type = 'text';
				}
			} else if(!empty($headers['NEID']) &&  !empty($headers['Call Type']) && $data[$headers['Call Type']] == 'VoCDMA' && $this->type=='talk' && (empty($data[$headers['DIALED_DIGITS']]) || (false !== strpos($data[$headers['DIALED_DIGITS']],'@'))) && (($data[$headers['NEID']] >= 191 && $data[$headers['NEID']] <= 198) || ($data[$headers['NEID']] >= 226 && $data[$headers['NEID']] <= 229) || ($data[$headers['NEID']] >= 291 && $data[$headers['NEID']] <= 294) || ($data[$headers['NEID']] >= 540 && $data[$headers['NEID']] <= 559))){
				$this->type = 'text';
			} else if(empty($towers)){
			// Call also has no towers, therefore the call failed
				if(!empty($headers['MOBILE ROLE'])){
					$data[$headers['MOBILE ROLE']] = 'FAILED';
				}
				if($this->type=='text'){
					$this->type = 'talk';
				}
			}
		} else {
			if($this->type == 'text'){
				$this->type = 'talk';
			}
		}
		if(!empty($headers['NEID'])){
			if(0 === strpos($data[$headers['NEID']],'6205')){
				$this->type = 'talk';
				$this->forwarding = (isset($data[$headers['CALLED_NBR']])? self::cleannumber($data[$headers['CALLED_NBR']]) : (isset($data[$headers['DIALED_DIGITS']])? self::cleannumber($data[$headers['DIALED_DIGITS']]) : '')).'(FVM)';
			}
			if(false !== strpos($data[$headers['NEID']],'6245000') || (isset($headers['Call Type']) && $data[$headers['Call Type']] == 'VoVoice')){
				// VoVoice
				$this->type = 'talk';
				$this->forwarding = (isset($data[$headers['CALLED_NBR']])? self::cleannumber($data[$headers['CALLED_NBR']]) : (isset($data[$headers['DIALED_DIGITS']])? self::cleannumber($data[$headers['DIALED_DIGITS']]) : '')).'(F)';
			}
		}
		if(isset($headers['Call Type']) && $this->type == 'talk' && ($data[$headers['Call Type']] == 'VoVoip' || $data[$headers['Call Type']] == 'VoVoice')){
			if(strtotime($this->conn_date) >= strtotime('04-02-2018') && strtotime($this->conn_date) <= strtotime('28-02-2018')){
				$this->warn_date = '*';
			}
			if(strtotime($this->conn_date) >= strtotime('31-05-2017') && strtotime($this->conn_date) <= strtotime('19-10-2017')){
				// Convert timestamp from GMT (we already default to UTC)
				$this->original_timezone = 'UTC';
			}			
		}
		if(!empty($headers['MOBILE ROLE']) && $data[$headers['MOBILE ROLE']] == 'FAILED'){
			$this->term = isset($data[$headers['CALLED_NBR']])? self::cleannumber($data[$headers['CALLED_NBR']]) : (isset($data[$headers['DIALED_DIGITS']])? self::cleannumber($data[$headers['DIALED_DIGITS']]) : '');
		}
		
		$this->loc = '[';
		if(!empty($towers)){
			$this->tower_count = count(array_unique(array_column($towers,'id')));
			$host = 'https://mapware.net/denis/api_fn1.html';
			$params = [];
			$coordstring = "";
			for($i=0; $i < count($towers); $i++){
				$tower = $towers[$i];
				$obj = SprintTowers::where('id',$tower['id'])->where('sector',$tower['sector'])->first();
				$label = 'Tower';
				if(!empty($obj)){
					if(!isset($obj->azimuth)||empty($obj->azimuth)){
						$obj->azimuth = '-1';
					}
					if(strtolower($obj->bts)=='ericsson'){
						switch(trim($tower['sector'])){
							case '2':
								$tower['sector'] = 0;
								break;
							case '3':
								$tower['sector'] = 180;
								break;
							case '4':
								$tower['sector'] = 270;
								break;
							default:
								$tower['sector'] = 90;
								break;								
						}
					} else {
						switch(trim($tower['sector'])){
							case '1':
								$tower['sector'] = 0;
								break;
							case '2':
								$tower['sector'] = 180;
								break;
							case '3':
								$tower['sector'] = 270;
								break;
							default:
								$tower['sector'] = 90;
								break;								
						}
					}
					$this->loc .= str_replace(['[',']'],'',$obj->neid).':'.$obj->azimuth.':'.$obj->longitude.':'.$obj->latitude.':'.str_replace(['[',']'],'',$tower['sector']).':,';
					//$this->loc .= $obj->latitude.':'.$obj->longitude.'::'.$obj->sector.',';
					if($i==0 || $i==(count($towers)-1)){
						$coordstring .= ($obj->latitude).','.($obj->longitude);
						$coordstring .= ",".$label.",Blue,".$obj->azimuth."|";
					}
				} else {
					// If we do not have that tower in the database, we need to return a failure message and request file upload
					$this->towers = false;
					return false;
				}
			}
			if($this->tower_count > 1){
				$this->highlight = "#DAF4FA";
			}
			$params['towerRange'] = 2;//$this->tower_count;
				if(!empty($coordstring)){
					$params['points'] = rtrim($coordstring,'|');
				}
				if(isset($params['points'])){
					$paramstring = '';
					foreach($params as $k=>$p){
						$paramstring .= $k.'='.$p.'&';
					}
					$this->loc_link[] = $host.'?'.rtrim($paramstring,'&');
				}
		}
		if($this->type == 'text'){
			if(strtotime($this->conn_date) > strtotime('20-10-17')){
				// Convert timestamp from CT
				$is_dst = new DateTime($this->conn_date.' '.$this->region.'/'.$this->city.'');
				if(!$is_dst->format('I')){
					$this->original_timezone = 'CST';
				} else {
					$this->original_timezone = 'CDT';
				}
			}
			if(!empty($headers['NEID']) && ($data[$headers['NEID']] >= 540 && $data[$headers['NEID']] <= 559) && (strtotime($this->conn_date) <= strtotime('19-10-17') && strtotime($this->conn_date) >= strtotime('31-05-17'))){
				// Convert timestamp from GMT (we already default to UTC)
				$this->original_timezone = 'UTC';
			}
		}
		if(trim($this->term)=='911'){
			$this->highlight = '#D4FFEC';
		}
		$this->loc = rtrim($this->loc,',');
		$this->loc .= ']';		
		
        if($type=="data"||$type=="data_no_loc"){
			$this->bu = !empty($headers['BYTES_UP'])&&!empty($data[$headers['BYTES_UP']])? $data[$headers['BYTES_UP']] : '';
			$this->bu_orig = $this->bu;
			$this->bd = !empty($headers['BYTES_DOWN'])&&!empty($data[$headers['BYTES_DOWN']])? $data[$headers['BYTES_DOWN']] : '';
			$this->bd_orig = $this->bd;
			
				if($this->bu >= 1000000000){
					$this->bu = number_format($this->bu / 1000000000,1)."G";
				} else if($this->bu >=1000000){
					$this->bu = number_format($this->bu / 1000000,1)."M";
				} else if($this->bu >=1000){
					$this->bu = number_format($this->bu / 1000,1)."K";
				} else {
					$this->bu .= "B";
				}
				if($this->bd >= 1000000000){
					$this->bd = number_format($this->bd / 1000000000,1)."G";
				} else if($this->bd >=1000000){
					$this->bd = number_format($this->bd / 1000000,1)."M";
				} else if($this->bd >=1000){
					$this->bd = number_format($this->bd / 1000,1)."K";
				} else {
					$this->bd .= "B";
				}
        }
		Phone::firstOrCreate(['phone'=>$this->origin]);
		Phone::firstOrCreate(['phone'=>$this->term]);
    }

	public function cleannumber($number){
			$number = str_replace("[<=9999999]",'',$number);
			return str_replace('-','',$number);
	}


}

