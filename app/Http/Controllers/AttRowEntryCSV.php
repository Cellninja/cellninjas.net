<?php

namespace App\Http\Controllers;
use App\Phone as Phone;
use App\Http\Controllers\AttRowEntry as AttRowEntry;
use Illuminate\Support\Facades\Log;

class AttRowEntryCSV extends AttRowEntry{
	
	public $highlight="white;display:none";
	public $loc_link=[];
    /**
     * Create a new  instance.
     *
     * @return void
     */
    public function __construct($array,$type)
    {
        //$this->item = $array[0]->textContent;
        $this->type = $type;
		$this->tower_count = 0;
		$this->loc_end = $this->loc = [];
        switch($type){
            case "talk":{
				$i = 0;
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->seizure=$array[$i];
                $i++;
                $this->et=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->term=$array[$i];
                $i++;
                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->ct=$array[$i];
                $i++;
                $this->feat=$array[$i];
                $i++;
				$this->dialed=$array[$i];
				$i++;
				$this->forwarded=$array[$i];
				$i++;
				$this->translated=$array[$i];
				$i++;
				$this->orig_orig=$array[$i];
				$i++;
				$this->make=$array[$i];
				$i++;
				$this->model=$array[$i];
				$i++;
                if(isset($array[$i])){
					$locations_array = [];
					for($z=(count($array)-1); $z>=$i; $z--){
						$locations_array[] = $array[$z];
					}
					$reverse_locations = array_filter($locations_array, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
					});
					if(!empty($reverse_locations)){
						$this->loc_end=$reverse_locations[0];
						$this->loc=array_reverse($reverse_locations);
					}
                }
                break;
            }
            case "data":{
				$i = 0;
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->et=$array[$i];
                $i++;
                $this->bu=$array[$i];
                $this->bu_orig = $this->bu;
				$i++;
                $this->bd=$array[$i];
                $this->bd_orig = $this->bd;
				$i++;
				
				if(strlen($this->bu) >=10){
					$this->bu = number_format($this->bu / 1000000000,1)."G";
				} else if(strlen($this->bu) >=7){
					$this->bu = number_format($this->bu / 1000000,1)."M";
				} else if(strlen($this->bu) >=4){
					$this->bu = number_format($this->bu / 1000,1)."K";
				} else {
					$this->bu .= "B";
				}

				if(strlen($this->bd) >=10){
					$this->bd = number_format($this->bd / 1000000000,1)."G";
				} else if(strlen($this->bd) >=7){
					$this->bd = number_format($this->bd / 1000000,1)."M";
				} else if(strlen($this->bd) >=4){
					$this->bd = number_format($this->bd / 1000,1)."K";
				} else {
					$this->bd .= "B";
				}

                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->make=$array[$i];
                $i++;
                $this->model=$array[$i];
                $i++;
                if(isset($array[$i])){
					$locations_array = [];
					for($z=(count($array)-1); $z>=$i; $z--){
						$locations_array[] = $array[$z];
					}
					$reverse_locations = array_filter($locations_array, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
					});
					if(!empty($reverse_locations)){
						$this->loc_end=$reverse_locations[0];
						$this->loc=array_reverse($reverse_locations);
					}
                }
                break;
            }
            case "text":{
				$i = 0;
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->term=$array[$i];
                $i++;
                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->desc=$array[$i];
                $i++;
                $this->feat=$array[$i];
                $i++;
                $this->make=$array[$i];
                $i++;
                $this->model=$array[$i];
                $i++;
                if(isset($array[$i])){
					$locations_array = [];
					for($z=(count($array)-1); $z>=$i; $z--){
						$locations_array[] = $array[$z];
					}
					$reverse_locations = array_filter($locations_array, function($value) {
							return ($value !== null && $value !== false && $value !== ''); 
					});
					if(!empty($reverse_locations)){
						$this->loc_end=$reverse_locations[0];
						$this->loc=array_reverse($reverse_locations);
					}
                }
                break;
            }
            case "talk_no_loc":{
				$i = 0;
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->seizure=$array[$i];
                $i++;
                $this->et=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->term=$array[$i];
                $i++;
                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->ct=$array[$i];
                $i++;
                $this->feat=$array[$i];
                $i++;
				$this->dialed=$array[$i];
				$i++;
				$this->forwarded=$array[$i];
				$i++;
				$this->translated=$array[$i];
				$i++;
				$this->orig_orig=$array[$i];
				$i++;
				$this->make=$array[$i];
				$i++;
				$this->model=$array[$i];
                break;
            }
            case "data_no_loc":{
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->et=$array[$i];
                $i++;
                $this->bu=$array[$i];
                $this->bu_orig = $this->bu;
				$i++;
                $this->bd=$array[$i];
                $this->bd_orig = $this->bd;
				$i++;
				
				if(strlen($this->bu) >=10){
					$this->bu = number_format($this->bu / 1000000000,1)."G";
				} else if(strlen($this->bu) >=7){
					$this->bu = number_format($this->bu / 1000000,1)."M";
				} else if(strlen($this->bu) >=4){
					$this->bu = number_format($this->bu / 1000,1)."K";
				} else {
					$this->bu .= "B";
				}

				if(strlen($this->bd) >=10){
					$this->bd = number_format($this->bd / 1000000000,1)."G";
				} else if(strlen($this->bd) >=7){
					$this->bd = number_format($this->bd / 1000000,1)."M";
				} else if(strlen($this->bd) >=4){
					$this->bd = number_format($this->bd / 1000,1)."K";
				} else {
					$this->bd .= "B";
				}

                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->make=$array[$i];
                $i++;
                $this->model=$array[$i];
                break;
            }
            case "text_no_loc":{
				$i = 0;
                $this->item=$array[$i];
				$i++;
                $this->conn_date=$array[$i];
                $i++;
                $this->conn_time=$array[$i];
                $i++;
                $this->origin=$array[$i];
                $i++;
                $this->term=$array[$i];
                $i++;
                $this->imei=$array[$i];
                $i++;
                $this->imsi=$array[$i];
                $i++;
                $this->desc=$array[$i];
                $i++;
                $this->feat=$array[$i];
                $i++;
                $this->make=$array[$i];
                $i++;
                $this->model=$array[$i];
                break;
            }
        }
		if(trim($this->term)=='911'){
			$this->highlight = '#D4FFEC';
		}
		if(!empty($this->dialed)){
			$this->forwarding = $this->dialed.'(D)';
		}
		if(!empty($this->forwarded)){
			if(!empty($this->forwarding)){
				$this->forwarding .= ' '.$this->forwarded.'(F)';
			}
			$this->forwarding = $this->forwarded.'(F)';
		}
		if(!empty($this->orig_orig)){
			if(!empty($this->forwarding)){
				$this->forwarding .= ' '.$this->orig_orig.'(OO)';
			}
			$this->forwarding = $this->orig_orig.'(OO)';
		}
		Phone::firstOrCreate(['phone'=>$this->origin]);
		Phone::firstOrCreate(['phone'=>$this->term]);
    }



}

