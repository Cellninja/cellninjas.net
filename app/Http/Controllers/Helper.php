<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use App\Phone as Phone;
use App\Geocode as Geocode;

class Helper {

    /**
     * Helper function to get the address from a lat,long pair, using the Google Maps API.
     *
     */

    public static function geocode($latlng,$result=null){
		if(!empty($result)){
			return $result;
		}
		$latlng = str_replace(' ','',$latlng);
        set_error_handler(
            function ($severity, $message, $file, $line) {
                throw new \ErrorException($message, $severity, $severity, $file, $line);
            }
        );
        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$latlng."&sensor=true&key=".config('pdfs.key.google');
        try {
			// check if there's already a response stored for that location in the database
			$resp_json = Geocode::where('latlng',$latlng)->first();

			if(!empty($resp_json)){
				$resp_json = $resp_json->response;
			}
			if(empty($resp_json)){
				// store response in database
				$resp_json = file_get_contents($url);
				$resp = json_decode($resp_json, true);
				if($resp['status']=='OK'){
					Geocode::firstOrCreate(['latlng'=>$latlng,'response'=>$resp_json]);
				}
			}
            $resp = json_decode($resp_json, true);
            $geocoded = "";
            if($resp['status']=='OK'){
                $street = $resp['results'][0]['address_components'][0]['short_name'];
                $route = $resp['results'][0]['address_components'][1]['short_name'];
                $locality = $resp['results'][0]['address_components'][2]['short_name'];
				$admin_area = "";
                if(isset($resp['results'][0]['address_components'][4])){
                    $admin_area = $resp['results'][0]['address_components'][4]['short_name'];
                }
				$geocoded = "$street $route, $locality, $admin_area";
                return $geocoded;
	    }
	    return false;
	} catch (\Exception $e) {
		return false;
        }
    }

	public static function offsetUTC($tz){
		switch($tz){
			case 'EDT':
				$tz = '-400';
				break;				
			case 'EST':
			case 'CDT':
				$tz = '-500';
				break;
			case 'CST':
			case 'MDT':
				$tz = '-600';
				break;
			case 'MST':
			case 'PDT':
				$tz = '-700';
				break;
			case 'PST':
			case 'AKDT':
				$tz = '-800';
				break;
			case 'AKST':
				$tz = '-900';
				break;			
			default:
				$tz = $tz;
				break;
		}
		return $tz;
	}

	public static function offsetUTCReverse($tz){
		switch($tz){
			case '+0':
			case '-0':
				$tz = 'UTC';
				break;
			case '-4':
				$tz = 'EDT';
				break;				
			case '-5':
				//$tz = 'EST';
				$tz = 'CDT';
				break;
			case '-6':
				//$tz = 'CST';
				$tz = 'MDT';
				break;
			case '-7':
				//$tz = 'MST';
				$tz = 'PDT';
				break;
			case '-8':
				//$tz = 'PST';
				$tz = 'AKDT';
				break;
			case '-9':
				$tz = 'AKST';
				break;			
			default:
				$tz = $tz;
				break;
		}
		return $tz;
	}
	
}