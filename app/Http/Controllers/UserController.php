<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $user = User::find($request->get('id'));

        $enabled = $request->get('enabled');
        if(isset($enabled)) {
            $user->enabled = $enabled;
        }

        $code = $request->get('code');
        if(isset($code)) {
            $user->code = $code;
        }

        $plan_id = $request->get('plan_id');
        if(isset($plan_id)) {
            $user->plan_id = $plan_id;
        }

        $user->save();

        return redirect('/users')->with('success', 'User updated successfully!');
    }
}
