<?php

/**
  * Step 0: postPDF handles file upload, analyzes the mime-type, and passes through corresponding business logic.
  * 
  * Step 1: PDF Upload Userflow: processUploadedPDF
  * Step 1: CSV Upload Userflow: processUploadedCSV
  *
  * Step 1.1 Subroutine: PDF Upload Userflow: getNumber
  * Step 1.1 Subroutine: CSV Upload Userflow: getNumber
  *
  * Step 1.2 Subroutine: CSV Upload Userflow: RowEntryCSV (object)
  * Step 1.3 Subroutine: CSV Upload Userflow: processNewRow
  * Step 1.4.1 Subroutine: CSV Upload Userflow: generateFinalHTML (pdf)
  * Step 1.4.2 Subroutine: CSV Upload Userflow: saveOriginalToExcel (csv)
  *
  * Step 2: PDF Upload Userflow: getRawHTML
  * Step 2.1 Subroutine: getPDFPages
  *
  * Step 3: PDF Upload Userflow: postHtml (invoked by ajax)
  * Step 3.a: PDF Upload Userflow: getProcessedFile (subroutine)
  *
  * Step 4: PDF Upload Userflow: processGeneratedHTML
  * Step 4.1: getPDFPages
  * Step 4.2: getNumber
  * Step 4.3: DOMDocument (object)
  * Step 4.4: DOMDocument->loadHTMLFile
  * Step 4.5: getPDFRow
  * Step 4.6: RowEntry (object)
  * Step 4.7: processNewRow
  * Step 4.8.1: generateFinalHTML
  * Step 4.8.2: CSV saveOriginalToExcel
  **/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Storage;
use App\Http\Controllers\AttRowEntry;
use App\Http\Controllers\AttRowEntryCSV;
use App\Http\Controllers\SprintRowEntryCSV;
use App\Http\Controllers\TMobileRowEntryCSV;
use App\Http\Controllers\VerizonRowEntryCSV;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use mikehaertl\wkhtmlto\Pdf as WKPDF;
use App\Geocode as Geocode;

use PhpOffice\PhpSpreadsheet\Spreadsheet as Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Xlsx;
use Illuminate\Support\Facades\Artisan as Artisan;

class PdfController extends Controller
{
    public $calculated_geo = [];
	public $calculated_timezone = [];
	public $timezone_acronym = [];
    public $tower_list = [];
    public $rows_array = [];
    public $distances_array = [];
    public $html_to_print_array = [];
    public $mime_type="";
    public $default_timezone="UTC";
	public $existing_timezone="";
    public $total_data=0;
	public $total_voice=0;
	public $total_text=0;
	public $total_from="";
	public $total_to="";
	public $total_duration="";
	public $current_page=1;
	public $total_pages=0;
	public $orig_page_count=0;
	public $report_title="";
	public $total_phone="";
    public $starting_page = 1;
	public $last_row_day = "";
	public $current_hour = "";
	public $current_day = "";
	public $last_day_items = 0;
	public $last_hour_items = 0;
    public $global_cursor_add=0;
    public $temp_cont=0;
    public $apikey;
	public $storagePath;
	public $temp;
	public $pdfs;
	public $mutool;
	public $uploadedPdfLog = 'debugdec2';
    public $excel_headers;
    public $excel_titles = ['talk'=>'Voice','text'=>'SMS','data'=>'Data','talk_no_loc'=>'Voice','text_no_loc'=>'SMS','data_no_loc'=>'Data'];
	public $carrier;
	public $device = '';

    //Settings for splitting larger files
    public $split_in_rows_amount = 100;
    public $split_filename;
    public $split_temporal_html_filename;
    public $split_reference_filename;
    public $split_reference_array = [];
    public $final_part = false;

    //Debugging settings
    public $limit_pages=false;
    public $debugSkipHTML=false;
    public $noCleaup=true;
    public $start_row="1";
    public $debug_start=1;
	public $debug_end=5;
	public $debug_file;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->middleware('auth');
		$this->split_filename = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp')."levitan_split";
		$this->split_temporal_html_filename = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp')."levitan_split_html";
		$this->split_reference_filename = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp')."levitan_split_reference";
		$this->debug_file = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp').'out3.pdf';
		$this->excel_headers = 	[	
									'talk'=>\Config::get('constants.TALK_HEADERS'),
									'text'=>\Config::get('constants.TEXT_HEADERS'),
									'data'=>\Config::get('constants.DATA_HEADERS'),
									'talk_no_loc'=>\Config::get('constants.TALK_HEADERS'),
									'text_no_loc'=>\Config::get('constants.TEXT_HEADERS'),
									'data_no_loc'=>\Config::get('constants.DATA_HEADERS')
							];
    }


    /**
     * Process the uploaded PDF file
	 *
	 * PDF Upload Userflow: Step 1 (processUploadedPDF)
     */
    public function processUploadedPDF($path,$address,$mode="pdf",$carrier="att",$case="CellNinja",$date='null',$time='null',$timestamp='null'){
        $original_pdf_name = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().$path;
        file_put_contents(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp').DIRECTORY_SEPARATOR.$this->uploadedPdfLog,$original_pdf_name);
		$fsize = filesize($original_pdf_name);
		$cmd = 'php '.base_path().DIRECTORY_SEPARATOR.'artisan process:pdf '.$original_pdf_name.' '.$mode.' '.$carrier.' '.$address.' "'.$case.'" "'.$date.'" "'.$time.'" "'.$timestamp.'" > /dev/null 2>&1 &';
		if(null!==getenv('APP_ENV')&&getenv('APP_ENV')==='development'){
			$cmd = 'php '.str_replace('\\', '/', base_path().DIRECTORY_SEPARATOR).'artisan process:pdf '.str_replace('\\', '/', $original_pdf_name).' '.$mode.' '.$carrier.' '.$address.' "'.$case.'" "'.$date.'" "'.$time.'" "'.$timestamp.'"';
		}
		$process = Process::fromShellCommandline($cmd);
		$process->start();
		if($fsize <= config('pdfs.limit.size')){
			$this->getRawHTML($original_pdf_name);
			return $path;
		}
		return false;
    }
	
	/**
	 * Process the uploaded Sprint or Verizon towers file
	 *
	 */
	public function processTowerUploadCSV($path,$carrier){
		if($carrier === 'sprint'){
			$original_file_name = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."$path";
			if(null!==getenv('APP_ENV')&&getenv('APP_ENV')==='development'){
				Artisan::call('import:sprinttowers',['filename'=>str_replace('\\', '/', $original_file_name)]);
			}
			Artisan::call('import:sprinttowers',['filename'=>$original_file_name]);
			return Artisan::output();
		} else if($carrier === 'verizon'){
			$original_file_name = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."$path";
			if(null!==getenv('APP_ENV')&&getenv('APP_ENV')==='development'){
				Artisan::call('import:verizontowers',['filename'=>str_replace('\\', '/', $original_file_name)]);
			}
			Artisan::call('import:verizontowers',['filename'=>$original_file_name]);
			return Artisan::output();
		}
		return false;
	}

    /**
     * Process the uploaded CSV file
	 *
	 * CSV Upload Userflow: Step 1 (processUploadedCSV)
     */
    public function processUploadedCSV($path,$address,$mode="pdf",$carrier="att",$case="CellNinja",$date='null',$time='null',$timestamp='null'){
        ini_set("auto_detect_line_endings", "1");
        
        $original_pdf_name = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."$path";
		
 		$fsize = filesize($original_pdf_name);
		$cmd = 'php '.base_path().DIRECTORY_SEPARATOR.'artisan process:txt '.$original_pdf_name.' '.$mode.' '.$carrier.' '.$address.' "'.$case.'" "'.$date.'" "'.$time.'" "'.$timestamp.'" > /dev/null 2>&1 &';
		if(null!==getenv('APP_ENV')&&getenv('APP_ENV')==='development'){
			$cmd = 'php '.str_replace('\\', '/', base_path().DIRECTORY_SEPARATOR).'artisan process:txt '.str_replace('\\', '/', $original_pdf_name).' '.$mode.' '.$carrier.' '.$address.' "'.$case.'" "'.$date.'" "'.$time.'" "'.$timestamp.'"';
		}
		$process = Process::fromShellCommandline($cmd);
		$process->start();
		if($fsize <= config('pdfs.limit.size')){
			return $path;
		}
		return false;
    }
	
	public function processGeneratedCsv($filename,$mode='pdf',$carrier='att'){
		$this->carrier = $carrier;
		switch($carrier){
			case 'tmobile':
				return self::processTMobileCsv($filename,$mode);
				break;
			case 'sprint':
				return self::processSprintCsv($filename,$mode);
				break;
			case 'verizon':
				return self::processVerizonCsv($filename,$mode);
				break;
			case 'att':
			default:
				return self::processAttCsv($filename,$mode);
				break;
		}
	}
	
	public function processAttCsv($filename,$mode='pdf'){
		// REFACTOR: We aren't actually reading XLS/XLSX files at all for ATT, so all those "in_array" conditions really only apply to CSV.
		$row_init = $this->start_row;
		$rows = [];
		$page_types=[];
		$prev_type="";
		$read_rows = false;
		$cols = 13;
		$current_type = "";
		
		$handle = fopen($filename, 'r');     
		$i=0;
		while (false !== ($data = fgets($handle))) {
			if(empty($data)){
				continue;
			}
			if (strpos($data, 'Voice Usage') !== false) {
				$type = "talk";
				$cols = 18; // Has a bunch of columns that aren't in pdf
					// REFACTOR: Set in config variables
			}
			if (strpos($data, 'Data Usage') !== false) {
				$type = "data";
				$cols = 13;
					// REFACTOR: Set in config variables
			}
			if (strpos($data, 'SMS Usage') !== false) {
				$type = "text";
				$cols = 13;	// Has Make and Model columns
					// REFACTOR: Set in config variables
			}
			if (strpos($data, 'SMS-Email Usage') !== false) {
				$type = "email";
				$cols = 8;	// Has Make and Model columns
					// REFACTOR: Set in config variables
				continue;
			}
			if(empty($this->total_phone)&&strpos($data,'Usage')!==false){
				$number = $this->getNumber($data);
				$this->total_phone = !empty($number)? $number[0] :'';
			}
			if (strpos($data, 'Item') !== false) {
				// For some silly reason, the TXT files merge "Conn Date" and "Conn Time" into 1 column "ConnDateTime", so this aims to fix that in the header.
				// Later on, the data will need to be fixed
				$connDateTimeFlag = false;
				if(strpos($data, 'ConnDateTime')!==false){
					$data = str_replace("ConnDateTime","ConnDate,ConnTime",$data);
					$connDateTimeFlag = true;
				}
				$column_header = explode(',',trim($data));
				if(count($column_header)>$cols){
					return false;
				}
				$page_type_append_sa=in_array("CellLocation",$column_header)||in_array("Cell Location",$column_header)?"":"_no_loc";
				$page_type_append_sa.=(in_array("ApnNetwork",$column_header)||in_array("Apn",$column_header))?"_network":"";
				$current_type = $type.$page_type_append_sa;
				$read_rows = true;
			} else if($read_rows==true && count(explode(',',trim($data)))>3){
				if($connDateTimeFlag == true){
					// Find the position of the date
					$match = preg_match('/\d{2}\/\d{2}\/\d{2}\\s\d{2}/',$data,$regex_matches);
					if($match){
						foreach($regex_matches as $item){
							$item2 = str_replace(' ',',',$item);
							$data = str_replace($item,$item2,$data);
						}
					}
				}
				$new = new AttRowEntryCSV(explode(',',trim($data)),$current_type);
				$this->processNewRow($new,$current_type);
				if(!empty($new->make)&&!empty($new->model)){
					$this->device = $new->make.' '.$new->model;
				}
				if($this->total_phone=="undefined"){
					$number = $new->origin_p;
					$this->total_phone = $number;
				}
			}
			$i++;
		}
		fclose($handle);
		if($mode=="excel"){
			return $this->saveOriginalToExcel($this->rows_array);
		}
		ksort($this->rows_array);
		return $this->generateFinalHTML();	// Returns final filename without path
	}

	public function processTMobileCsv($filename,$mode='pdf'){
		$row_init = null;
		$rows = [];
		$page_types=[];
		$prev_type="";
		$read_rows = false;
		$cols = 36;
		$current_type = "";
		
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$sheet = $reader->listWorksheetNames($filename)[0];
		$reader->setLoadSheetsOnly($sheet);
		$spreadsheet = $reader->load($filename);
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		$headers = [];
		foreach($sheetData as $row=>$data){
			if(empty($data)|| ($data['A']!=="Date" && empty($row_init))){
				continue;
			}
			if(empty($row_init)){
				$row_init = $row;
			}
			if($row==$row_init){
				$headers = array_flip(array_filter($sheetData[$row_init]));
				continue;
			}
			if(count($headers)>$cols){
				return false;
			}
			if(empty($this->total_phone) && !empty($headers['Calling Number']) && !empty($headers['Direction'])){
				if(!empty($data[$headers['Calling Number']])){
					$this->total_phone = $data[$headers['Calling Number']]; // First outgoing "calling number"
				}
			}
			$type = 'data';
			if(!empty($data[$headers['Call Type']])){
				switch($data[$headers['Call Type']]){
					case 'callForwarding':
					case 'mSOriginating':
					case 'mSTerminating':
					case 'moc':
					case 'mtc':
						$type = 'talk';
						break;
					case 'mSOriginatingSMSinMSC':
					case 'mSTerminatingSMSinMSC':
					case 'SMSC':
						$type = 'text';
						break;
					case 'RCS-IMCHAT';
						$type = 'mms';
					default:
						$type = 'data'; // No records of this in sample file
						break;
				}
			}
			$new = new TMobileRowEntryCSV($row,$data,$type,$headers);
			$this->processNewRow($new,$type);
			if($this->total_phone=="undefined" || empty($this->total_phone)){
				$this->total_phone = $new->origin_p;
			}
		}
		if($mode=="excel"){
			return $this->saveOriginalToExcel($this->rows_array);
		}
		ksort($this->rows_array);
		return $this->generateFinalHTML();	// Returns final filename without path
	}
		
	public function processSprintCsv($filename,$mode='pdf'){
		// Here I'm using XLS strictly by hard-coding it instead of CSV.
		$row_init = $this->start_row;
		$rows = [];
		$page_types=[];
		$prev_type="";
		$read_rows = false;
		$cols = 11;
		$current_type = "";
		
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$this->total_phone = $reader->listWorksheetNames($filename)[0];
		//$reader->setReadDataOnly(true);
		$reader->setLoadSheetsOnly($this->total_phone);
		$spreadsheet = $reader->load($filename);
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		$headers = array_flip(array_filter($sheetData[$row_init]));
		foreach($sheetData as $row=>$data){
			if(empty($data)||$row<2){
				continue;
			}
			if(count($headers)>$cols){
				return false;
			}
			foreach(['CALLING_NBR','CALLED_NBR','DIALED_DIGITS','MOBILE ROLE','START_DATE','END_DATE','DURATION (SEC)','Call Type','NEID','1ST CELL','LAST CELL'] as $h){
				if(!isset($headers[$h])){
					return false;
				}
			}
			$type = 'data';
			if(!empty($data[$headers['Call Type']])){
				switch($data[$headers['Call Type']]){
					case 'Voice':
					case 'VoVoice':
					case 'VoWiFi':
					case 'VoCDMA':
					case 'VoLTE':
					case 'Airave':
					case 'VoVoip':
						$type = 'talk';
						break;
					case 'WiFi':
					case 'Text Detail':
						$type = 'text';
						break;
					default:
						$type = 'data';
						break;
				}
			}
			$new = new SprintRowEntryCSV($row,$data,$type,$headers);
			if(isset($new->original_timezone)){
				$this->default_timezone = $new->original_timezone;
			}
			if($new->towers===false){
				return false;
			}
			$this->processNewRow($new,$type);
			if($this->total_phone=="undefined"){
				$this->total_phone = $new->origin_p;
			}
		}
		if($mode=="excel"){
			return $this->saveOriginalToExcel($this->rows_array);
		}
		ksort($this->rows_array);
		return $this->generateFinalHTML();	// Returns final filename without path
	}

	public function processVerizonCsv($filename,$mode='pdf'){
		$rows = [];
		$page_types=[];
		$prev_type="";
		$read_rows = false;
		$cols = 4;
		$current_type = "";
		
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$sheets = $reader->listWorksheetNames($filename);
		foreach($sheets as $sheet){
			$row_init = null;
			switch(strtolower($sheet)){
				case 'volte':
					$type = 'talk';
					break;
				case 'tdr2':
					$type = 'data';
					break;
				default:
					$type = 'talk';
					break;
			}
			$reader->setLoadSheetsOnly($sheet);
			$spreadsheet = $reader->load($filename);
			$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			$headers = [];
			foreach($sheetData as $row=>$data){
				if(empty($data)|| ($type == 'talk' && $data['A']!=="Record Open Date/Time" && empty($row_init)) || ($type == 'data' && $data['A']!=="Start Date/Time" && empty($row_init))){
					continue;
				}
				if(empty($row_init)){
					$row_init = $row;
				}
				if($row==$row_init){
					$headers = array_flip(array_filter($sheetData[$row_init]));
					continue;
				}
				if(count($headers)<$cols){
					return false;
				}
				if($type=='talk'){
					foreach(['Record Open Date/Time','Cell ID','Cell Face','Dir','MSISDN','Called #'] as $h){
						if(!isset($headers[$h])){
							return false;
						}
					}
				}
				if($type=='data'){
					foreach(['Start Date/Time','Stop Date/Time','Orig Total MB','Cell ID'] as $h){
						if(!isset($headers[$h])){
							return false;
						}
					}
				}
				if(empty($this->total_phone) && !empty($headers['MSISDN'])){
					$this->total_phone = $data[$headers['MSISDN']]; // First outgoing "calling number"
				}
				$new = new VerizonRowEntryCSV($row,$data,$type,$headers);
				$this->processNewRow($new,$type);
				if($this->total_phone=="undefined" || empty($this->total_phone)){
					$this->total_phone = $new->origin_p;
				}
			}
		}
		if($mode=="excel"){
			return $this->saveOriginalToExcel($this->rows_array);
		}
		ksort($this->rows_array);
		return $this->generateFinalHTML();	// Returns final filename without path
	}
		
	public function processGeneratedPdf($path,$mode='pdf',$carrier='att'){
		$this->carrier = $carrier;
		switch($carrier){
			case 'tmobile':
				return self::processTMobilePdf($path,$mode);
				break;
			case 'sprint':
				return self::processSprintPdf($path,$mode);
				break;
			case 'verizon':
				return self::processVerizonPdf($path,$mode);
				break;
			case 'att':
			default:
				return self::processAttPdf($path,$mode);
				break;
		}
	}
	
	public function processTMobilePdf($path,$mode='pdf'){
		// TMobile does not allow PDFs
		return false;
	}	

	public function processSprintPdf($path,$mode='pdf'){
		// Sprint does not allow PDFs
		return false;
	}

	public function processVerizonPdf($path,$mode='pdf'){
		// Verizon does not allow PDFs
		return false;
	}	
		
	public function processAttPdf($path,$mode='pdf'){
        $this->orig_page_count=$this->getPDFPages($path);
        $row_init = $this->start_row;
        $rows = [];
        $page_types=[];
        $prev_type="";
        $page_start =  $this->limit_pages?$this->debug_start:1;
        $page_end =  $this->limit_pages?$this->debug_end:$this->orig_page_count;
		$this->total_phone = "";

		$headers = [];
        for ($i = $page_start; $i <= $page_end; $i++) {
			$page_one = false;
			$ofn = basename($path, '.pdf');
            $pdf_name = $ofn."_output_".$i.".html";
            $html_file_path = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp')."$pdf_name";
            $html_string = file_get_contents($html_file_path);
			/**
				Sometimes files will contain pages which are not part of the AT&T report at the beginning or end of the report. Every file's valid pages will contain "usage for", so everything else can be skipped.
			**/
			if(strpos(strtolower($html_string),'usage for')===false){
				continue;
			} else if($page_one == false){
				$page_one = true;
				$number = $this->getNumber($html_string);
				$this->total_phone = $number[0];	
			}
			$cols = 11;	// REFACTOR: Set in env variables
			$page_type = "text";
            if (strpos($html_string, 'Voice Usage') !== false) {
                $page_type = "talk";
				$cols = 12;	// REFACTOR: Set in env variables
            }
            elseif(strpos($html_string, 'Data Usage') !== false){
                $page_type = "data";
				$cols = 13;	// REFACTOR: Set in env variables
            }            
			elseif(strpos($html_string, 'SMS-Email Usage') !== false){
                $page_type = "email";
				$cols = 8;	// REFACTOR: Set in env variables
				continue;
            }

            if (strpos($html_string, 'location') == false) {
                $page_type .= "_no_loc";
            }

            if($prev_type!=$page_type){
                if($prev_type==""){
                    $prev_type=$page_type;
                }
                else{
                    $prev_type=$page_type;
                    $row_init = strval("1");
                }
            }

            $doc = new \DOMDocument();
			try{
				$doc->loadHTMLFile($html_file_path);
				$xpath = new \DOMXpath($doc);
				$entriesDOMNodeList = $xpath->query("//p");
				$entries = iterator_to_array($entriesDOMNodeList);
				$stop = false;
				$headers = self::getHeaders($entries);
				if(count($headers)>$cols){
					return false;
				}
				while(!$stop){
					$ret = $this->getPDFRow($row_init,$entries);
					$row_init+=1;
					$row_init = strval($row_init);
					if(empty($ret)){
						$stop = true;
					}
					else{
						array_push($rows,$ret);
						array_push($page_types,$page_type);
						//$stop = true;
					}
				}
			} catch(Exception $e){
				while(!$stop){
					$row_init+=1;
					$row_init = strval($row_init);
					$stop = true;
				}
			}
			$row_init=strval($row_init-1);

        }

        $debug_active = false;
		$temptype = !empty($page_types)? $page_types[0] : 'text';
		$iterator = 0;
        foreach($rows as $key=>$r){
			if($temptype !== $page_types[$key]){
				$iterator = 0;
			}
            $new = new AttRowEntry($r,$page_types[$key],$iterator,$mode);
			if(!empty($new)){
				$this->processNewRow($new,$page_types[$key]);
				if(!empty($new->make)&&!empty($new->model)){
					$this->device = $new->make.' '.$new->model;
				}
			} else if(isset($last)){
				$itm = (($last->item+1) < 10)? '0'.($last->item+1) : ($last->item+1);
				$this->rows_array[strtotime($last->conn_date." ".$last->conn_time).$itm.$page_types[$key]] = new AttRowEntry('warning',$page_types[$key],$iterator,$mode);
			}
			$last = $new;
			$temptype = $page_types[$key];
			$iterator++;
        }
        if($mode=="excel"){
            $filename = $this->saveOriginalToExcel($this->rows_array); // Returns final filename without path
			ksort($this->rows_array);
        } else {
            ksort($this->rows_array);
            unset($rows);
            $filename = $this->generateFinalHTML();	// Returns final filename without path
		}
		return $filename;
	}

    /**
     * Step 4: PDF Upload Userflow. This is the final.
     */
    public function processGeneratedHTML($path,$type='pdf',$mode='pdf',$carrier='att'){
		if(in_array($type, ["text/plain","text/x-csv","application/vnd.ms-excel","text/csv","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","csv","txt","xls","xlsx"])){
			return self::processGeneratedCsv($path,$mode,$carrier);	// This will process the file from HTML to PDF or XLS and return the filename without path
		} else {
			return self::processGeneratedPdf($path,$mode,$carrier);	// This will process the file from HTML to PDF or XLS and return the filename without path
		}
    }
	
	public function isValidSheet($mime_type, $carrier = 'att'){
		$valid_formats = ["application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","xlsx"];
		if(!in_array($carrier, ['sprint','verizon'])){
				$valid_formats[] = "xls";
				$valid_formats[] = "txt";
				$valid_formats[] = "csv";
				$valid_formats[] = "text/x-csv";
				$valid_formats[] = "text/plain";
				$valid_formats[] = "text/csv";
		}
		return in_array($mime_type, $valid_formats);
	}

    /**
     * Process an uploaded PDF or CSV/TXT file. This is a user accessible endpoint.
	 *
	 * PDF Upload Userflow: Step 0 (postPDF)
	 * CSV Upload Userflow: Step 0 (postPDF)
     *
     * @return JSON
     */
    public function postPDF(Request $request)
    {
        $ret = "";
		$address = $request->user()->email;
        $this->report_title = $request->input('name');

        $mode = !empty($request->input('mode'))? $request->input('mode') : 'pdf';
        $source = !empty($request->input('source'))? $request->input('source') : 'records';
		$carrier = !empty($request->input('carrier'))? strtolower($request->input('carrier')) : 'att';
		$date = !empty($request->input('date'))? strtolower($request->input('date')) : null;
		$time = !empty($request->input('time'))? strtolower($request->input('time')) : null;
		$timestamp = !empty($request->input('timestamp'))? strtolower($request->input('timestamp')) : date("Y-m-d h:ia");
		
        if ($request->hasFile('pdfFile')) {
            $file = $request->file('pdfFile');
			
            if(!$file->isValid()) {
                return response()->json([
                    'status' => 'error',
                    'values' => 'invalid file upload: '.$file->getErrorMessage()
                ],400);
            }
			$directory = config('pdfs.path.pdfs');
            $ret = $file->store($directory);
			// Gets the filename by parsing the full file path starting from the end of the path (gets 0th position and adds length of directory path)
			$filename = substr($ret,strpos($ret,$directory)+(strlen($directory)+1));
			$this->mime_type = pathinfo(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().$ret, PATHINFO_EXTENSION);

            if(in_array($this->mime_type,["application/pdf","application/octet-stream","pdf"])){
				if($carrier==='sprint' || $carrier==='tmobile' || $carrier==='verizon'){
                    return response()->json([
                        'status' => 'error',
                        'values' => 'invalid file upload: '.$this->mime_type
                    ],400);					
				}
				// REFACTOR: This was originally just 3 parameters and now it's grown to too many... these should be moved to an array.
                $filename = $this->processUploadedPDF($ret,$address,$mode,$carrier,!empty($this->report_title)? $this->report_title : null,$date,$time,$timestamp);
				if(false===$filename){
					return response()->json([
                        'status' => 'ok',
                        'original' => 'large'
                    ]);
				}
            } else if(self::isValidSheet($this->mime_type, $carrier)){
					if($source == 'towers'){
						if(!in_array($carrier,['sprint','verizon'])){
							return response()->json([
								'status' => 'error',
								'original' => 'response',
								'message' => 'Tower import is only available for Sprint Legacy or Verizon data. Be sure you are selecting the correct carrier from the carrier dropdown.'
							],400);							
						}
						$result = $this->processTowerUploadCSV($ret,$carrier);
						if($result === false){
							return response()->json([
								'status' => 'error',
								'original' => 'response',
								'message' => 'Unable to import tower data, please check your file formatting and try again.'
							],400);
						} else {
							return response()->json([
								'status' => 'ok',
								'original' => 'response',
								'message' => 'Towers have been imported, please refresh and restart your cell record upload process.'
							]);
						}
					} else {
						$filename = $this->processUploadedCSV($ret,$address,$mode,$carrier,!empty($this->report_title)? $this->report_title : null,$date,$time,$timestamp);
					}
					$this->cleanUp();
					if(false===$filename){
						return response()->json([
							'status' => 'ok',
							'original' => 'large'
						]);
					}
            } else {
                    return response()->json([
                        'status' => 'error',
                        'values' => 'invalid file upload: '.$this->mime_type
                    ],400);
            }			
			return response()->json([
				'status' => 'ok',
				'original' => $filename
			]);
        }
		return response()->json([
			'status' => 'error',
			'original' => 'unable to upload file. please try again.'
		]);
    }

    /**
     * Step 3:  PDF Upload Userflow. This is a user accessible endpoint.
	 *			Initiates the file generation process
     *
     * @return JSON
     */

    public function postHTML(Request $request)
    {
        $file = $request->input('original');
        $this->report_title = !empty($request->input('name')) ? $request->input('name') : null;
		$mode = !empty($request->input('mode'))? $request->input('mode') : 'pdf';
		$carrier = !empty($request->input('carrier'))? strtolower($request->input('carrier')) : 'att';
		$case = !empty($request->input('name'))? $request->input('name') : '';
		$date = !empty($request->input('date'))? strtolower($request->input('date')) : null;
		$time = !empty($request->input('time'))? strtolower($request->input('time')) : null;
		$timestamp = !empty($request->input('timestamp'))? strtolower($request->input('timestamp')) : date("Y-m-d h:ia");
		
		$url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."/$file";
		$filename = $this->getProcessedFile($url,$mode,$carrier,$case,$date,$time,$timestamp);
		$err_msg = ($carrier == 'att' && $mode == 'pdf')? 'There is a formatting issue with AT&T’s pdf file. To resolve this issue, request a csv file format from AT&T, and run this report again' : 'Your file contains invalid formatting. Please check to see that you have the correct columns in your report.';
		$cause = 'reports';
		if(in_array($carrier,['sprint','verizon']) && $mode == 'pdf'){
			$err_msg = 'Your file contains invalid formatting or missing cell towers. Please upload your cell towers file (XLSX format) now or check your column report and refresh and reload the page to upload a new one.';
			$cause = 'towers';
		}
		if($filename===false){
			return response()->json([
				'status' => 'error',
				'cause' => $cause,
				'values' => $err_msg
			],415);
			// This is a cheat code we use for passing custom errors back when the return value should otherwise have been false
		} else if(false !== strpos($filename, 'error:')){
			$err_msg = str_replace('error:','',$filename);
			return response()->json([
				'status' => 'error',
				'cause' => 'towers',
				'values' => $err_msg
			],415);
		}
		$file = Storage::disk('s3')->url(config('services.s3.output').$filename);
        return response()->json([
            'status' => 'ok',
            'url' => $file,
            'name' => $filename
        ]);
    }
	
    /**
     * Step 3.a: PDF Upload Userflow. This is a user accessible endpoint.
     *
     * @return JSON
     */

    public function getProcessedFile($url,$mode='pdf',$carrier="att",$case="CellNinja",$date=null,$time=null,$timestamp=null)
    {
		$type = pathinfo($url, PATHINFO_EXTENSION);
		$this->report_title = $case;
		$this->report_date = !empty($date)? str_replace('-', '/', $date) : null;
		$this->report_time = $time;
		$this->report_timestamp = $timestamp;
        $filename = $this->processGeneratedHTML($url,$type,$mode,$carrier);
		$this->cleanUp();
		return $filename;
	}
	
	/**
	 *	This function is intended to retrieve file headers for each data type.
	 *  This gets used by validation logic to ensure that the file has the correct columns.
	 *
	 */
	
	public function getHeaders($array){
		/**
		Because the PDF's aren't encoded properly, we have to reassemble what the headers should look like.  We do this by doing the following:
		(1) Loop through each row(cell) to (a) remove all style attributes from each row and (b) find/replace <p><tt><b><span> and </span></b</tt></p> with a single space " ".
		(2) If the value of the cell is all lowercase OR contains parentheses OR equals "date", "time", or "number", trim left.
		**/
		$headerstring = '';
		if(!empty($array)) {
		  $array = array_slice($array,0,50); 	// REFACTOR: Set in env variables
		  foreach($array as $element) {
			if(is_numeric(preg_split('#\s+#', $element->textContent, 2)[0])){
				break;	// All headers are alphanumeric, so the first numeric string is the 1st row.
			}
			$headerstring .= (ctype_lower($element->textContent) || str_replace(['(',')'], '', $element->textContent) !== $element->textContent || in_array($element->textContent,['Date','Time','Number','Up','Dn','Network','Type','Location']))? (ctype_lower($element->textContent)? "":" ").trim($element->textContent) : "  ".trim($element->textContent);
		  }
		}
		$headers = explode("  ",trim($headerstring));
		return $headers;
	}

    /**
     * This function separates the extracted NodeList object into logical RowEntry objects.
     * The RowEntry objects are the ones that are actually written to the final report.
     *
	 * Since the PDF parsing isn't properly evaluating the column separation, we will combine everything
	 * into a single column and allow the RowEntry function to parse using regex
     */

    public function getPDFRow($row, &$entries){
        $start = false;
        $elements = "";
        $next_row = $row + 1;
        $next_row = strval($next_row);
        $prev_top ="";
        $to_delete = [];
        foreach($entries as $key => $n){
            $text1 = $n->textContent;
            $text2 ="";
            $text3_temp= explode(" ",$n->textContent,2);
            $text3 ="";
            $text3 = $text3_temp[0];
            if(count($entries)>$key+1){
                $text2 = $n->textContent . $entries[$key+1]->textContent;
            }
            if((strval($text1) === $row || strval($text2) === $row || strval($text3) === $row) && $start == false){
                $start = true; // This flag indicates that the parsing may begin.
            }
            if($start==true){
                //check if end of page
                $style = $n->getAttribute('style');
                if (($pos = strpos($style, "top:")) !== FALSE) {
                    $top_temp = explode("pt;",substr($style, strpos($style, "top:") + 4),2);
                    $top = $top_temp[0];
                }
                else{
                    $top = "0";
                }
                if($prev_top==""){
                    $prev_top = $top;
                }
                else{
                    if($prev_top-100>$top){
                        $start = false;
                        break;
                    }
                }

                //get id if 2 lines
                $text1 = $n->textContent;
                $text2 = "";
                //get id if in same row as date
                if(count($entries)>=$key+1&&!empty($entries[$key+1])){
                    $text2 = $n->textContent . $entries[$key+1]->textContent;
                }

                //check if ID coincidentally appears in a node in the row. If so ignore it.
                $left = $this->getLeft($n);
                $too_far_left = $left > 50;

                if(($text1 === $next_row || $text2 === $next_row || $text3 === $next_row)&&!$too_far_left){
                    $start = false;
                    break;
                }
                else{
					/**
					 * Compile all row values, double-space delimited, without their styling atts
					 * We'll use regular expressions to determine where the breaks should be in the RowEntry file.
					*/
					$text1 = trim($text1);
					if(!isset($text1) || $text1 === ''){
						continue;
					}
					$elements .= $text1."  ";
                    $to_delete[] = $key;
                }
            }
        }
        foreach($to_delete as $delete){
            unset($entries[$delete]);
        }
        $entries = array_values($entries);
        return $elements;
    }

    /**
     * Helper function to get the left value from each row in HTML.
     *
     */

    private function getLeft($n){
        $style = $n->getAttribute('style');
        if (($pos = strpos($style, "left:")) !== FALSE) {
            $left_temp = explode("pt;",substr($style, strpos($style, "left:") + 5),2);
            $left = $left_temp[0];
        }
        return $left;
    }

    /**
     * Helper function to get the timezone from a lat,long pair, using the Google Maps API.
     *
     */

    public function getTimezone($latlng,$timestamp){
	$latlng = str_replace(' ','',$latlng);
        set_error_handler(
            function ($severity, $message, $file, $line) {
                throw new \ErrorException($message, $severity, $severity, $file, $line);
            }
        );
        try {
            $url = "https://maps.googleapis.com/maps/api/timezone/json?location=".$latlng."&timestamp=$timestamp&key=".config('pdfs.key.google');
			$resp_json = file_get_contents($url);
			$resp = json_decode($resp_json, true);
            if($resp['status']=='OK'){
				return $resp;
                //$timezone = $resp['timeZoneId'];
                //return $timezone;
            }
	    else{
                return false;
            }
        }
	catch (\Exception $e) {
            return false;
        }

    }

	function getNext(&$array, $curr_key)
	{
		$next = 0;
		reset($array);

		do
		{
			$tmp_key = key($array);
			$res = next($array);
		} while ( ($tmp_key != $curr_key) && $res );

		if( $res )
		{
			$next = key($array);
		}

		return $next;
	}

	function getPrev(&$array, $curr_key)
	{
		end($array);
		$prev = key($array);

		do
		{
			$tmp_key = key($array);
			$res = prev($array);
		} while ( ($tmp_key != $curr_key) && $res );

		if( $res )
		{
			$prev = key($array);
		}

		return $prev;
	}


    /**
     * This function writes the final PDF report
     * Parameters to replace from the template:
     * total_records
     * total_from
     * total_to
     * total_duration
     * total_phone
     * report_title
     * total_voice
     * total_text
     * total_data
     */
    public function generateFinalHTML(){
        $rows = $this->rows_array;

        $total_records = number_format(count($rows));
        $total_voice = number_format($this->total_voice);
        $total_text = number_format($this->total_text);
        $total_data = number_format($this->total_data);

        $total_phone = str_replace(')',') ',$this->total_phone); //input
        $report_title = $this->report_title; //input
		
        $template_path = \Config::get('view.paths')[0].'/templates/template.html';
        $template = file_get_contents($template_path);
		$carrier = ucfirst($this->carrier);
		if($carrier=='Att'){
			$carrier = 'AT&T';
		}
		if($carrier=='Tmobile'){
			$carrier = 'T Mobile';
		}
		$device = $this->device;
		$incident_date = (!empty($this->report_date))? date("F j",strtotime($this->report_date)) : '';
		$incident_time = (!empty($this->report_time))? date("g:i a",strtotime($this->report_time)) : '';

        $cursor = 171;
        $temp_text = "";

        $prev_tz = null;

        $this->starting_page = $this->current_page;
        $day_page=$this->current_page;
        $hour_page=$this->current_page;
        $day_items=$this->last_day_items;
        $hour_items=$this->last_hour_items;
        $county=0;

		$search_header  = array("{{total_records}}","{{carrier}}","{{total_phone}}","{{report_title}}","{{total_voice}}","{{total_text}}","{{total_data}}","{{incident_date}}","{{incident_time}}","{{device}}");
		$replace_header = array($total_records,$carrier,$total_phone,$report_title,$total_voice,$total_text,$total_data,$incident_date,$incident_time,$device);
		$temp_text = $this->processTemplate('header',$search_header,$replace_header);
		$cursor = 114;
		if(empty($rows)){
			return false;
		}
		$i = 0;
		foreach($rows as $key=>$row){
            $tz = $row->timezone!=""?$row->timezone:$this->default_timezone;
            if($row->timezone!=""){
                $tz = $row->timezone;
                $prev_tz = $row->timezone;
            }else{
                if($prev_tz != null){
                    $tz = $prev_tz;
                    $prev_tz = $prev_tz;
                }
                else{
                    $tz = $this->default_timezone;
                    $prev_tz = $prev_tz;
                }
            }
			if($tz==='UTC'){
				$now_key = $key;
				for($z=$i; $z<count($rows); $z++){
					$next_key = self::getNext($rows, $now_key);
					if(isset($rows[$next_key]) && $rows[$next_key]->timezone !== $tz){
						$row->timezone = $prev_tz = $tz = $rows[$next_key]->timezone;
						break;
					}
					$now_key = $next_key;
				}
			}
			if($tz==='UTC'){
				$now_key = $key;
				for($x=count($rows); $x>=0; $x--){
					$next_key = self::getPrev($rows, $now_key);
					if($rows[$next_key]->timezone !== $tz){
						$row->timezone = $prev_tz = $tz = $rows[$next_key]->timezone;
						break;
					}
					$now_key = $next_key;
				}
			}
			// REFACTOR: Temporarily blocking Verizon since we don't have files to work with.
			if($tz==='UTC' && $carrier !== 'AT&T' && $carrier !== 'Verizon'){
				return 'error:Invalid towers.  Report does not have cell tower locations.  Please request records with cell towers from the cellphone company or enter a timezone to proceed without locations.';
			}
			if($tz=='CORRECT' || (isset($row->original_timezone) && $row->original_timezone=='CORRECT')){
				$temp_date = new \DateTime($row->time_p);
			} else {
				$tz = Helper::offsetUTC($tz);
				$temp_date = new \DateTime($row->time_p, new \DateTimeZone('UTC'));
				$temp_date->setTimezone(new \DateTimeZone($tz));
			}
            $row_day = $temp_date->format('l, F j, Y');
            $row_hour = $temp_date->format('g:00 A');

            if( ($cursor>\Config::get('constants.PAGE.PAGE_MAX'))
            || (($this->current_day!=$row_day && $this->current_day!="")&&($cursor+62>=\Config::get('constants.PAGE.PAGE_MAX')))
            || (($this->current_hour!=$row_hour && $this->current_hour!="")&&($cursor+31>=\Config::get('constants.PAGE.PAGE_MAX')))
            ){
                $search_template  = array("{{REPLACE}}","{{current_page}}");
                $replace_template  = array($temp_text,$this->current_page);
                $pdf_text = str_replace($search_template,$replace_template,$template);
                $this->html_to_print_array[$this->current_page] = $pdf_text;
                $this->current_page = $this->current_page+1;

                $search_header  = array("{{phone_number}}","{{date_cont}}");
                $replace_header = array($total_phone,$row_day);
                $temp_text = $this->processTemplate('header2',$search_header,$replace_header);
                //aquí de una vez hay que agregar el otro coso
                $cursor = 54;
            }
            if(($this->current_day=="" || $this->current_day!=$row_day)){
                //finalizar texto de día y hora anterior
                if($day_page!=$this->current_page){
                    $search_template  = array("{{day_items}}");
                    $replace_template  = array($day_items);
                    $this->html_to_print_array[$day_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$day_page]);
                }
                else{
                    $search_template  = array("{{day_items}}");
                    $replace_template  = array($day_items);
                    $temp_text = str_replace($search_template,$replace_template,$temp_text);
                }
                if($hour_page!=$this->current_page){
                    $events_text = $hour_items != 1 ? "events" : "event";
                    $search_template  = array("{{hour_items}}","{{events}}");
                    $replace_template  = array($hour_items,$events_text);
                    $this->html_to_print_array[$hour_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$hour_page]);
                }
                else{
                    $events_text = $hour_items != 1 ? "events" : "event";
                    $search_template  = array("{{hour_items}}","{{events}}");
                    $replace_template  = array($hour_items,$events_text);
                    $temp_text = str_replace($search_template,$replace_template,$temp_text);
                }

                $this->current_day = $row_day;
                if($this->total_from==""){
                    $this->total_from=$row_day;
                }
                $temp_text.= $this->getDayToPDF($cursor-10,$row_day,'942');         //llenar la cantidad para este día
                $cursor+= 31;
                $temp_text.= $this->getHourToPDF($cursor-10,$row_day,'1',$row_hour); //llenar con la cantidad de eventos para esta hora
                $cursor+= 31;
                $this->current_hour = $row_hour;
                $day_items= 0;
                $hour_items= 0;
                $day_page = $this->current_page;
                $hour_page = $this->current_page;
            }
            if(($this->current_hour=="" || $this->current_hour!=$row_hour)){
                if($hour_page!=$this->current_page){
					$events_text = $hour_items != 1 ? "events" : "event";
					$search_template  = array("{{hour_items}}","{{events}}");
					$replace_template  = array($hour_items,$events_text);
					$this->html_to_print_array[$hour_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$hour_page]);
                }
                else{
                    $events_text = $hour_items != 1 ? "events" : "event";
                    $search_template  = array("{{hour_items}}","{{events}}");
                    $replace_template  = array($hour_items,$events_text);
                    $temp_text = str_replace($search_template,$replace_template,$temp_text);
                }

                $temp_text.= $this->getHourToPDF($cursor-10,$row_day,'1',$row_hour); //llenar con la cantidad de eventos para esta hora
                $cursor+=31;
                $this->current_hour = $row_hour;
                $hour_items=0;
                $hour_page = $this->current_page;
            }
            $day_items+=1;
            $hour_items+=1;
            $temp_text.= $this->getRowToPDF($cursor,$row,$prev_tz);
            $cursor+=38;
            $cursor+=$this->global_cursor_add;
			$i++;
        }

        $search_template  = array("{{REPLACE}}","{{current_page}}");
        $replace_template  = array($temp_text,$this->current_page);
        $pdf_text = str_replace($search_template,$replace_template,$template);
        $this->html_to_print_array[$this->current_page] = $pdf_text;

        if($day_page != $this->current_page){
            $search_template  = array("{{day_items}}");
            $replace_template  = array($day_items);
            $this->html_to_print_array[$day_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$day_page]);
        }
        if($hour_page != $this->current_page || $this->current_hour===$row_hour){
            $events_text = $hour_items != 1 ? "events" : "event";
            $search_template  = array("{{hour_items}}","{{events}}");
            $replace_template  = array($hour_items,$events_text);
            $this->html_to_print_array[$hour_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$hour_page]);
        }

        $this->last_row_day = $row_day;
        $this->last_day_items = $day_items;
        $this->last_hour_items = $hour_items;

		$this->total_pages += count($this->html_to_print_array);
		$this->replaceHTMLPlaceholders();
		return $this->writePDF();
    }

    /**
     * Replace placeholder values in the HTML pages
     *
     */

    public function replaceHTMLPlaceholders(){
        $this->total_to=$this->last_row_day;
        $date_from = new \DateTime($this->total_from);
        $date_to = new \DateTime($this->total_to);
        $difference = $date_from->diff($date_to);
        $this->total_duration = ($difference->days+1).((($difference->days+1)<=1)? ' Day' : ' Days'); //calculado
        //Add the totals to our HTML strings. vik
        $search_template  = array("{{total_from}}","{{total_to}}","{{total_duration}}");
        $replace_template  = array($this->total_from,$this->total_to,$this->total_duration);
        $this->html_to_print_array[$this->starting_page] = str_replace($search_template,$replace_template,$this->html_to_print_array[$this->starting_page]);
        foreach($this->html_to_print_array as &$html_page){
            $search_template  = array("{{total_pages}}","{{run_time}}","{{report_title}}");
            $replace_template  = array($this->total_pages,gmdate("h:ia \U\T\C m/d/y"),$this->report_title);
            $html_page = str_replace($search_template,$replace_template,$html_page);
        }
    }

    /**
     * Actually writes the PDF files
     *
     */

    public function writePDF(){
        $options = array(
            'page-height' => '9in',
            'page-width' => '7in', //con 9 fijo sale en 1 página pero la página es muy grande
            'margin-bottom' => 0,
            'margin-left' => 0,
            'margin-right' => 0,
            'margin-top' => 0,
            'orientation' => 'Landscape',
            'commandOptions' => ['useExec' => true]
        );
		$name = str_replace(['(',')','-',' '],'',$this->total_phone);
		$filename = $name.'.pdf';

        if(count($this->html_to_print_array)>100){
            $pdfs_to_concat = "";
            for ($i = $this->starting_page; $i <= ceil(count($this->html_to_print_array)/100); $i++) {
                $pdf = new WKPDF($options);
                for ($j = 1+(($i-1)*100); $j <= 100+(($i-1)*100); $j++) {
                    if($j<=count($this->html_to_print_array)){
                        $pdf->addPage($this->html_to_print_array[$j]);
                    }
                }
                if (!$pdf->saveAs(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$name."_".$i.".pdf")) {
                    $error = $pdf->getError();
                }
                $pdfs_to_concat.=Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$name."_".$i.".pdf ";
            }
            exec(config('pdfs.tools.pdfunite')." $pdfs_to_concat ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$filename);
			Storage::disk('s3')->putFileAs(config('services.s3.output'), new File(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$filename), $filename);
			// exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$name."_*.pdf");
        }
        else{
            $pdf = new WKPDF($options);
            foreach($this->html_to_print_array as $page){
                $pdf->addPage($page);
            }
			if (!$pdf->saveAs(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$filename)){
                $error = $pdf->getError();
            } else {
				Storage::disk('s3')->putFileAs(config('services.s3.output'), new File(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$filename), $filename);
				// exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$name."_*.pdf");
			}
        }
		return $filename;
    }

    /**
     * Gets a Row as input and outputs the final HTML to write in the report.
     *
     */

    public function getRowToPDF($rowTop,$row,$prev_tz){
	
        if($row->timezone!=""){
            $tz = $row->timezone;
        }
        else{
            if($prev_tz != null){
                $tz = $prev_tz;
            }
            else{
                $tz = $this->default_timezone;
            }
        }
		//$tz = Helper::offsetUTC($tz);
        $type = $row->type;
        $tempdate = new \DateTime("05/13/17 06:41:54", new \DateTimeZone('UTC'));
        $tempdate->setTimezone(new \DateTimeZone($tz));
        $rowtz = $tempdate->format('T');
		if(strtolower($rowtz)=='utc'){
			$rowtz = '<span style="color:red;"><strong>'.$rowtz.'</strong></span>';
		}
		$startP = $row->getStartP($tz,(isset($row->original_timezone)&&$row->original_timezone=='CORRECT'));
		if(!empty($this->report_date) && !empty($row->getConnDate($tz)) && !empty($row->getEndP($tz)) && !empty($this->report_time)){
			$cndt = $row->getConnDate($tz,(isset($row->original_timezone)&&$row->original_timezone=='CORRECT'));
			$endPX = $row->getEndP($tz,(isset($row->original_timezone)&&$row->original_timezone=='CORRECT'));
			// NOTE: Make sure to get the 24 hour format in this date reformatting
			$scndt = (strtotime("$cndt $endPX") >= strtotime("$cndt $startP"))? $cndt : date('Y-m-d', strtotime("+1 day", strtotime($cndt) ));
			$occurrence = strtotime("$this->report_date $this->report_time");
			$start_timestamp = strtotime("$cndt $startP");
			$end_timestamp = strtotime("$scndt $endPX");
			if($start_timestamp < ($occurrence +59) && $occurrence < $end_timestamp && $row->highlight !== '#D4FFEC'){
				$row->highlight="#FFFEBE";
			}
		}
		if(isset($row->warn_date)){
			$startP = $row->warn_date.$startP;
		}
		// REFACTOR: I see a lot of default_timezone below... shouldn't they be using $tz?
		
        switch($type){
            case "talk_no_loc":
            case "talk":
                $rowTop2 = $rowTop + 13;
                $rowTop3 = $rowTop + 23;
                $rowTop4 = $rowTop + 26;

                $rowTop2b = $rowTop2;
                $this->global_cursor_add = 0;
                /*if(strlen($row->loc_p)>55){
                    $rowTop2b = $rowTop + 26;
                    $this->global_cursor_add = 13;
                    $rowTop4+=13;
                    $rowTop3+=13;
                }*/
                
                $tower_from=array_search($row->loc_p,$this->tower_list) +1;
                $tower_to=array_search($row->loc_end_p,$this->tower_list)+1;
				$distance = (empty($row->distance)||$row->distance==='0.00') ? "":$row->distance." miles Tower #".$tower_from." to #".$tower_to;
                $search_params  = array("{{row-top}}","{{row-top2}}","{{row-top2b}}","{{row-top3}}","{{row-top4}}","{{item}}","{{origin_p}}","{{term_p}}","{{forwarding_p}}","{{talk_time}}","{{duration}}","{{date_start}}","{{date_end}}","{{timezone}}","{{loc_start}}","{{loc_end}}","{{tower_count}}","{{highlight}}","{{dir_start}}","{{dir_end}}","{{distance_string}}","{{loc_link_first}}","{{loc_link_last}}");
                $replace_params = array($rowTop,$rowTop2,$rowTop2b,$rowTop3,$rowTop4,$row->item,$row->origin_p,$row->term_p,$row->forwarding_p,$row->et_p,$row->duration_p,$startP,$row->getEndP($tz),$rowtz,$row->loc_p,(($row->tower_count > 1)? $row->loc_end_p : ''),$row->tower_count,$row->highlight,$row->dir,(($row->tower_count > 1)? $row->dirFinal : ''),$distance,(isset($row->loc_link[0])? $row->loc_link[0] : '#'),(isset($row->loc_link[1])? $row->loc_link[1] : (isset($row->loc_link[0])? $row->loc_link[0] : '#')));
                $result = $this->processTemplate('talkrow',$search_params,$replace_params);
                return $result;
                break;
            case "data_no_loc":
            case "data":
                $rowTop2 = $rowTop + 13;
                $rowTop3 = $rowTop + 23;
                $rowTop4 = $rowTop + 26;

                $rowTop2b = $rowTop2;
                $this->global_cursor_add = 0;

                /*if(strlen($row->loc_p)>55){
                    $rowTop2b = $rowTop + 26;
                    $this->global_cursor_add = 13;
                    $rowTop4+=13;
                    $rowTop3+=13;
                }*/

                $tower_from=array_search($row->loc_p,$this->tower_list) +1;
                $tower_to=array_search($row->loc_end_p,$this->tower_list)+1;
                $distance = (empty($row->distance)||$row->distance==='0.00') ? "" : $row->distance." miles Tower #".$tower_from." to #".$tower_to;
		$search_params  = array("{{row-top}}","{{row-top2}}","{{row-top2b}}","{{row-top3}}","{{row-top4}}","{{item}}","{{origin_p}}","{{term_p}}","{{talk_time}}","{{duration}}","{{date_start}}","{{date_end}}","{{timezone}}","{{loc_start}}","{{loc_end}}","{{bu}}","{{bd}}","{{tower_count}}","{{highlight}}","{{dir_start}}","{{dir_end}}","{{distance_string}}","{{loc_link_first}}","{{loc_link_last}}");
                $replace_params = array($rowTop,$rowTop2,$rowTop2b,$rowTop3,$rowTop4,$row->item,$row->origin_p,$row->term_p,$row->et_p,$row->duration_p,$startP,$row->getEndP($tz),$rowtz,$row->loc_p,(($row->tower_count > 1)? $row->loc_end_p : ''),$row->bu,$row->bd,$row->tower_count,$row->highlight,$row->dir,(($row->tower_count > 1)? $row->dirFinal : ''),$distance,(isset($row->loc_link[0])? $row->loc_link[0] : '#'),(isset($row->loc_link[1])? $row->loc_link[1] : (isset($row->loc_link[0])? $row->loc_link[0] : '#')));
                $result = $this->processTemplate('datarow',$search_params,$replace_params);
                return $result;
                break;
            case "text_no_loc":
            case "text":
                $rowTop2 = $rowTop + 13;
                $rowTop3 = $rowTop + 20;
                $rowTop4 = $rowTop + 26;

                $rowTop2b = $rowTop2;
                $this->global_cursor_add = 0;

                /*if(strlen($row->loc_p)>55){
                    $rowTop2b = $rowTop + 26;
                    $this->global_cursor_add = 13;
                    $rowTop4+=13;
                    $rowTop3+=13;
                }*/

                $text_direction = "";
                $from=str_replace(' ','',$row->origin_p);
                $to=str_replace(' ','',$row->term_p);
                if($this->total_phone == $from){
                    $text_direction = "(Sent)";
                }
                if($this->total_phone == $to){
                    $text_direction = "(Received)";
                }

                $tower_from=array_search($row->loc_p,$this->tower_list) +1;
                $tower_to=array_search($row->loc_end_p,$this->tower_list)+1;
                $distance = (empty($row->distance)||$row->distance==='0.00') ? "" : $row->distance." miles Tower #".$tower_from." to #".$tower_to;
                $search_params  = array("{{row-top}}","{{row-top2}}","{{row-top2b}}","{{row-top3}}","{{row-top4}}","{{item}}","{{origin_p}}","{{term_p}}","{{date_start}}","{{date_end}}","{{timezone}}","{{loc_start}}","{{loc_end}}","{{tower_count}}","{{highlight}}","{{dir_start}}","{{dir_end}}","{{text_direction}}","{{distance_string}}","{{loc_link_first}}","{{loc_link_last}}");
                $replace_params = array($rowTop,$rowTop2,$rowTop2b,$rowTop3,$rowTop4,$row->item,$row->origin_p,$row->term_p,$startP,$row->getEndP($tz),$rowtz,$row->loc_p,(($row->tower_count > 1)? $row->loc_end_p : ''),$row->tower_count,$row->highlight,$row->dir,(($row->tower_count > 1)? $row->dirFinal : ''),$text_direction,$distance,(isset($row->loc_link[0])? $row->loc_link[0] : '#'),(isset($row->loc_link[1])? $row->loc_link[1] : (isset($row->loc_link[0])? $row->loc_link[0] : '#')));
                $result = $this->processTemplate('textrow',$search_params,$replace_params);
                return $result;
                break;
        }
    }

    /**
     * Outputs a day header for the final report
     *
     */

    public function getDayToPDF($bgTop,$day,$items){
        $rowTop = $bgTop + 11;
        $search_params  = array("{{bg-top}}","{{row-top}}","{{day}}");
        $replace_params = array($bgTop,$rowTop,$day);
        $result = $this->processTemplate('dayrow',$search_params,$replace_params);
        return $result;
    }

    /**
     * Outputs an hour header for the final report
     *
     */

    public function getHourToPDF($bgTop,$day,$items,$hour){
        $rowTop = $bgTop + 14;
        $search_params  = array("{{bg-top}}","{{row-top}}","{{day}}","{{hour}}");
        $replace_params = array($bgTop,$rowTop,$day,$hour);
        $result = $this->processTemplate('hourrow',$search_params,$replace_params);
        return $result;
    }

    /**
     * Processes a template file by replacing the arrays specified and returning the resulting string.
     *
     */

    public function processTemplate($name,$search_params,$replace_params){
        $template_path = \Config::get('view.paths')[0]."/templates/$name.html";
        $template = file_get_contents($template_path);
        $result = str_replace($search_params,$replace_params,$template);
        return $result;
    }

    /**
     * Converts the input PDF file into a series of HTML files for processing.
     *
	 * PDF Upload Userflow: Step 2 (getRawHtml)
     */

    public function getRawHTML($filename){
		$this->orig_page_count = $this->getPDFPages($filename);
		$ofn = basename($filename, '.pdf');
		$page_start =  $this->limit_pages?$this->debug_start:1;
		$page_end =  $this->limit_pages?$this->debug_end : $this->orig_page_count;
		for ($i = $page_start; $i <= $page_end; $i++) {
			exec(config('pdfs.tools.mutool')." draw -o ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp').$ofn."_output_".$i.".html $filename $i");
		}
    }

    /**
     * Cleans up temporary files used by the report generation process.
	 * REFACTOR: Run this in queue so that we don't have to wait for this to run during the process.
     *
     */

    public function cleanUp(){
        if(!empty($this->noCleaup)&&$this->noCleaup===true){
			return false;
        }
		// REFACTOR: Might be better to run these here than in the functions where they're run in-line throughout this file.
		if(getenv('APP_ENV')!=='development'){
			//exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().config('pdfs.path.temp')."*");
			//exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR."*.pdf");
			//exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR."*.xls");
		}
		return true;
    }

    /**
     * Returns the page count for an input PDF
     *
     */
    public function getPDFPages($document)
    {
        exec(config('pdfs.tools.pdfinfo')." ".$document, $output);
        $pagecount = 0;
        foreach($output as $op)
        {
            if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1)
            {
                $pagecount = intval($matches[1]);
                break;
            }
        }

        return $pagecount;
    }

    /**
     * Extracts a phone number from an input string. Used for automatically detecting the phone number from an input PDF.
	 *
	 * Step 1.1 Subroutine: PDF Upload Userflow: getNumber
	 * Step 1.1 Subroutine: CSV Upload Userflow: getNumber
     */
    public function getNumber($page){
        preg_match('/\(\d\d\d\)\d\d\d-\d\d\d\d/',$page,$matches);
        return $matches;
    }

    /**
     * Gets the distance between 2 lat long pairs by using the Haversine formula.
     *
     */
    public function getDistance($orig, $dest, $earthRadius = 6371000){
		$orig = str_replace(' ','',$orig);
		$dest = str_replace(' ','',$dest);
        // convert from degrees to radians
		if(false == strpos($orig,',')){
			$split_orig = explode(";",$orig,2);
		} else{
			$split_orig = explode(",",$orig,2);
		}
        $latitudeFrom = $split_orig[0];
        $longitudeFrom = $split_orig[1];

		if(false == strpos($orig,',')){
			$split_dest = explode(";",$dest,2);
		} else{
			$split_dest = explode(",",$dest,2);
		}
        $latitudeTo = $split_dest[0];
        $longitudeTo = $split_dest[1];

        $latFrom = deg2rad(floatval($latitudeFrom));
        $lonFrom = deg2rad(floatval($longitudeFrom));
        $latTo = deg2rad(floatval($latitudeTo));
        $lonTo = deg2rad(floatval($longitudeTo));

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
		$return_meters = $angle * $earthRadius;
        return number_format($return_meters*0.000621371192,1);
    }

    /**
     * Gets the distance between 2 lat long pairs by using the Google Maps API.
     *
     */
    public function getDistanceWithGoogle($orig,$dest){
        //Uses Google Maps Distance Matrix API
        //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=25.913115,-80.309318&destinations=25.896432,-80.14957&key=AIzaSyA3eC8C110ToXoUorETlRQCJZx4Vp4ljW0

        try {
            $url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$orig&destinations=$dest&key=".config('pdfs.key.google');
            $resp_json = file_get_contents($url);
            $resp = json_decode($resp_json, true);
	    $distance = "";
            if($resp['status']=='OK'){
                $distance_meters = $resp['rows'][0]['elements'][0]['distance']['value'];
                $distance = number_format(floatval($distance_meters) / 1609.344,1);
                if(array_key_exists($orig,$this->distances_array)){
                    $this->distances_array[$orig][$dest] = $distance;
                }
                else{
                    $this->distances_array[$orig] = [$dest => $distance];
                }

                if(array_key_exists($dest,$this->distances_array)){
                    $this->distances_array[$dest][$orig] = $distance;
                }else{
                    $this->distances_array[$dest] = [$orig => $distance];
				}
                return $distance;
            }
            else{
                echo "<strong>ERROR: {$resp['status']}</strong>";
                return false;
            }
        }
        catch (\Exception $e) {
            return "";
        }
    }

    /**
     * Initializes a newly created RowEntry object by calculating necessary fields based on the raw input.
     *
     */
    public function processNewRow($new,$current_type){
		if($new->item==''){
			return true;
		}
        switch($current_type){
            case "talk":{
                $this->total_voice +=1;
                break;
            }
            case "data":{
                $this->total_data +=1;
                break;
            }
            case "text":{
                $this->total_text +=1;
                break;
            }
            case "talk_no_loc":{
                $this->total_voice +=1;
                break;
            }
            case "data_no_loc":{
                $this->total_data +=1;
                break;
            }
            case "text_no_loc":{
                $this->total_text +=1;
                break;
            }

        }
	$new->calcFields();
	$new->distance = 0;
	if(!empty($new->latlng) && $new->latlng!="0.0,0.0"){
            if(array_key_exists($new->latlng.' '.$new->time_mdyh,$this->calculated_geo)){
                $new->loc_p = $this->calculated_geo[$new->latlng.' '.$new->time_mdyh];
                if(array_key_exists($new->latlng,$this->calculated_timezone)){
                    $this->existing_timezone = $this->calculated_timezone[$new->latlng];
                    $new->timezone=empty($new->timezone)? $this->existing_timezone : $new->timezone;
                } else{
                    $new->timezone=empty($new->timezone)? $this->default_timezone : $new->timezone;
                }
            } else {
                if(false !== ($res = Helper::geocode($new->latlng,$new->loc_p))){
					$this->calculated_geo[$new->latlng.' '.$new->time_mdyh]=$res;
					if($res!=''){
						$this->tower_list[]=$res;
					}
					$new->loc_p = $res;
				}
				$tz_def = date_default_timezone_get();
				// REFACTOR: Should we skip this is the timezone does not need to be adjusted? (ie. if "CORRECT")
				date_default_timezone_set('UTC');
				$timestamp = strtotime($new->time_p);
				date_default_timezone_set($tz_def);
				if(!empty($this->getTimezone($new->latlng,$timestamp))){
					$tz_object = $this->getTimezone($new->latlng,$timestamp);
					$this->existing_timezone = $tz_object['timeZoneId'];
					// Timezone String Fix
					if(!empty($tz_object['timeZoneName'])){
						if(false!==strpos(strtolower($tz_object['timeZoneName']),'alaska')){
							$tz_object['timeZoneName'] = str_replace('laska','las Ka',$tz_object['timeZoneName']);
						}
						$this->timezone_acronym[$tz_object['timeZoneId']] = preg_replace('/\b(\w)|./', '$1', $tz_object['timeZoneName']);
					}
        	        $this->calculated_timezone[$new->latlng]=$this->existing_timezone;
	                $new->timezone=empty($new->timezone)? $this->existing_timezone : $new->timezone;
				}
			}
	}
	if(!empty($new->latlngFinal) && $new->latlngFinal!="0.0,0.0"){
            if($new->latlng == $new->latlngFinal || empty($new->latlngFinal)){
				$new->loc_end_p = $new->loc_p;
			} else {
                if(array_key_exists($new->latlngFinal.' '.$new->time_mdyh,$this->calculated_geo)){
                    $new->loc_end_p = $this->calculated_geo[$new->latlngFinal.' '.$new->time_mdyh];
                    $new_dist="";
                    $already_calc=false;
                    if(array_key_exists($new->latlng,$this->distances_array)){
                        if(array_key_exists($new->latlngFinal,$this->distances_array[$new->latlng])){
                            $already_calc=true;
                            $new_dist = $this->distances_array[$new->latlng][$new->latlngFinal];
                        }
                    }
                    if($already_calc==false){
                        $new_dist = $this->getDistance($new->latlng,$new->latlngFinal);
                    }
                    $new->distance = !empty($new->loc_p)? number_format(floatval($new_dist),2) : 0.0;
                } else {
                    $res = Helper::geocode($new->latlngFinal,$new->loc_end_p);
                    $this->calculated_geo[$new->latlngFinal.' '.$new->time_mdyh]=$res;
                    if($res!=''){
                        $this->tower_list[]=$res;
					}
                    $new_dist = $this->getDistance($new->latlng,$new->latlngFinal);
                    $this->temp_cont++;
                    $new->distance = !empty($new->loc_p)? number_format(floatval($new_dist),2) : 0.0;
                    $new->loc_end_p = $res;
                }
            }
        }	
		if(isset($new->time_p)){
            $tempdate = new \DateTime($new->time_p, new \DateTimeZone('UTC'));
            $this->existing_timezone = empty($this->existing_timezone)?$this->default_timezone:$this->existing_timezone;
            $tempdate->setTimezone(new \DateTimeZone($this->existing_timezone));
			if(false==strpos($this->default_timezone,'/') && $this->default_timezone!=='UTC'){
				$this->timezone_acronym[$this->existing_timezone] = $this->default_timezone;
			}
			$new->timezone=!empty($new->timezone)? $new->timezone : (isset($this->timezone_acronym[$this->existing_timezone])? $this->timezone_acronym[$this->existing_timezone] : $tempdate->format('e'));
			if($new->item == 9 && $new->type == 'data'){ Log::debug(!empty($new->timezone)? 'new timezone' : (isset($this->timezone_acronym[$this->existing_timezone])? 'existing acronym' : 'new format')); }
            if($this->default_timezone=="UTC"){
				$this->default_timezone=$new->timezone;
            }
		}
		$itm = (($new->item+1) < 10)? '0'.($new->item+1) : ($new->item+1);
        $this->rows_array[strtotime($new->conn_date." ".$new->conn_time).$itm.$current_type] = $new;
    }

    /**
     * Saves the ouptut PDF (received as a RowEntry array) in Excel format... not sure why it is called saveOriginalToExcel... That is wrong
     *
     */
    public function saveOriginalToExcel($rows){
        
		$name = str_replace(['(',')','-',' '],'',$this->total_phone);
		$filename = $name.'.xls';
		$path = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR.$filename;
		
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $row_cont = 2;
        $row_prev_type="";
        foreach($rows as $row){
            $row_new_type = $row->type;			
            if($row_new_type!=$row_prev_type){
		    $case = (object)['type'=>'case','values'=>$this->report_title];
		    $this->writeExcelRow($sheet,$row_cont,$case);
                if($row_prev_type=="" && $row_cont==2){
                    $sheet->setTitle($this->excel_titles[$row_new_type]);
                }
                else{
                    $sheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $this->excel_titles[$row_new_type]);
                    $spreadsheet->addSheet($sheet);
                }
                foreach($this->excel_headers[$row_new_type] as $cell => $value){
                    $sheet->setCellValue($cell, $value);
                }
                $row_prev_type = $row_new_type;
                $row_cont = 2;
            }
            $this->writeExcelRow($sheet,$row_cont,$row);
            $row_cont++;
        }
		$case = (object)['type'=>'case','values'=>$this->report_title];
		$this->writeExcelRow($sheet,$row_cont,$case);
	        $writer = new Xlsx($spreadsheet);
		$writer->save($path);
		Storage::disk('s3')->putFileAs(config('services.s3.output'), new File($path), $filename);
		// exec("rm ".Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."public".DIRECTORY_SEPARATOR."*.xls");
		return $filename;
    }

    /**
     * Writes one RowEntry object into one Excel file row. See saveOriginalToExcel.
     *
     */
    function writeExcelRow($sheet,$row_cont,$row){
        switch($row->type){
			case "case":
				$sheet->setCellValueExplicit("G$row_cont",'Case: '.$row->values,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
			break;
            case "talk":
            case "talk_no_loc":
                $sheet->setCellValueExplicit("A$row_cont",$row->item,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("B$row_cont",$row->conn_date);
                $sheet->setCellValue("C$row_cont",$row->conn_time);
                $sheet->setCellValue("D$row_cont",$row->seizure);
                $sheet->setCellValue("E$row_cont",$row->et);
                $sheet->setCellValueExplicit("F$row_cont",$row->origin.(!empty($row->forwarding)? ' '.$row->forwarding : ''),
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("G$row_cont",$row->term,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("H$row_cont",$row->imei,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("I$row_cont",$row->imsi,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("J$row_cont",$row->ct);
                $sheet->setCellValue("K$row_cont",$row->feat);
                $sheet->setCellValue("L$row_cont",$row->loc);
            break;
            case "text":
            case "text_no_loc":
                $sheet->setCellValueExplicit("A$row_cont",$row->item,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("B$row_cont",$row->conn_date);
                $sheet->setCellValue("C$row_cont",$row->conn_time);
                $sheet->setCellValueExplicit("D$row_cont",$row->origin,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("E$row_cont",$row->term,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("F$row_cont",$row->imei,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("G$row_cont",$row->imsi,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("H$row_cont",$row->desc);
                $sheet->setCellValue("I$row_cont",$row->feat);
                $sheet->setCellValue("J$row_cont",$row->loc);
            break;
            case "data":
            case "data_no_loc":
                $sheet->setCellValueExplicit("A$row_cont",$row->item,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("B$row_cont",$row->conn_date);
                $sheet->setCellValue("C$row_cont",$row->conn_time);
                $sheet->setCellValueExplicit("D$row_cont",$row->origin,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("E$row_cont",$row->et);
                $sheet->setCellValueExplicit("F$row_cont",$row->bu_orig,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("G$row_cont",$row->bd_orig,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("H$row_cont",$row->imei,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValueExplicit("I$row_cont",$row->imsi,
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                );
                $sheet->setCellValue("J$row_cont",$row->make);
                $sheet->setCellValue("K$row_cont",$row->model);
                $sheet->setCellValue("L$row_cont",$row->loc);
            break;
        }
    }
}


