<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminMessage extends Mailable
{
    use SerializesModels;

    public $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$sender = (null!==getenv('MAIL_FROM_ADDRESS'))? getenv('MAIL_FROM_ADDRESS') : 'no-reply@cell.ninja';
		$key = (strpos(strtolower($this->file),'invalid')!==false)? 'error' : 'download';
		$text_template = ($key==='error')? 'admin_error_text' : 'admin_text';
		$template = ($key==='error')? 'admin_error' : 'admin';
        return $this->subject('Your '.null !== getenv('APP_NAME')? getenv('APP_NAME') : 'CellNinjas'.' Recorded Phone Records File')->from($sender)->view('mail.'.$template)->text('mail.'.$text_template)->with([$key=>$this->file,'appurl'=>getenv('APP_URL'),'appname'=>null !== getenv('APP_NAME')? getenv('APP_NAME') : 'CellNinjas']);
    }
}
