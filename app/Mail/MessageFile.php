<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageFile extends Mailable
{
    use SerializesModels;

    public $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$sender = (null!==getenv('MAIL_FROM_ADDRESS'))? getenv('MAIL_FROM_ADDRESS') : 'no-reply@cell.ninja';
		$key = (strpos(strtolower($this->file),'invalid')!==false)? 'error' : 'download';
		$text_template = ($key==='error')? 'pdf_error_text' : 'pdf_text';
		$template = ($key==='error')? 'pdf_error' : 'pdf';
        return $this->subject('Your '.null !== getenv('APP_NAME')? getenv('APP_NAME') : 'CellNinjas'.' Processed Phone Records File')->from($sender)->view('mail.'.$template)->text('mail.'.$text_template)->with([$key=>$this->file,'appurl'=>getenv('APP_URL'),'appname'=>null !== getenv('APP_NAME')? getenv('APP_NAME') : 'CellNinjas']);
    }
}
