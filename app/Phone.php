<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phone';
	public $incrementing = true;
    protected $fillable = [
        'phone'
    ];
}
