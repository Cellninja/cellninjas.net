<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Plan extends Model
{
    protected $table = 'plans';
	public $incrementing = true;
    protected $fillable = [
        'stripe_prod_id', 'stripe_plan_id', 'title'
    ];
	
	public function user()
	{
		return $this->hasMany(User::class);
	}
}
