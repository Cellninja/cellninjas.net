<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Validator::extend('allowed_domain', function($attribute, $value, $parameters, $validator) {
            return !in_array(explode('@', $value)[1], ['gmail.com','yahoo.com', 'hotmail.com', 'outlook.com']);
        }, 'Domain not valid for registration.');
        //
    }
}
