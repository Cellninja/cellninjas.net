<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/postPDF', 'PdfController@postPDF');
Route::post('/postHTML', 'PdfController@postHTML');

/*
    The eloquent default user only allows updating of the email, name, and
    password fields, an other fields will not be updated, because of this a
    separate controller needed to be created to handle updating those fields.
*/
Route::post('/editUser', 'UserController@update')->name('user.edit');
